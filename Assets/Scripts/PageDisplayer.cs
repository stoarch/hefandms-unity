﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PageDisplayer : MonoBehaviour {

	public Material[] pagesMaterials;
	public GameObject leftPage;
	public GameObject rightPage;
	public GameObject leftRotatingPage;
	public GameObject rightRotatingPage;
	public Material firstCoverPageMat;
	public Material lastCoverPageMat;
	public GameObject pageRotator;
	public Button prevButton;
	public Button nextButton;

	public int pageNo;

	void Start () {
		pageNo = 0;
		oldPageNo = 0;
		DisplayPages();
	}

	void ShowLeftPage()
	{
		int leafNo = pageNo*2;
		int oldLeafNo = oldPageNo*2;
		
		int leftLeafNo = oldLeafNo;
		int rightLeafNo = oldLeafNo + 1;
		
		if( oldPageNo > pageNo ) //rotate left to right
		{
			leftLeafNo = oldLeafNo - 2;
			rightLeafNo = oldLeafNo - 1;
		}
		
		if( leftLeafNo >= pagesMaterials.Length - 2 )
			leftLeafNo = pagesMaterials.Length - 2;
		if( rightLeafNo >= pagesMaterials.Length - 1 )
			rightLeafNo = pagesMaterials.Length - 1;
		
		leftLeafNo = leafNo - 1;
		rightLeafNo = leafNo;

		if( leftLeafNo < 0 )
		{
			leftPage.SetActive( false );
			
			return;
		}
		
		leftPage.SetActive( true );
		
		if( pageNo < pagesMaterials.Length - 1 )
		{
			var leftRenderer = leftPage.GetComponent<MeshRenderer>();
			leftRenderer.material = pagesMaterials[leftLeafNo];
			return;
		}
		else if( pageNo == pagesMaterials.Length - 1 ) //last page
		{
			var leftRenderer = leftPage.GetComponent<MeshRenderer>();
			leftRenderer.material = pagesMaterials[pageNo];

			return;
		}
		else 
		{
			Debug.Log( "PageDisplayer::ShowLeftPage :> Invalid page number " + pageNo.ToString() );
			return;
		}
	}

	void ShowRightPage()
	{
		int leafNo = pageNo*2;
		int oldLeafNo = oldPageNo*2;
		
		int leftLeafNo = oldLeafNo;
		int rightLeafNo = oldLeafNo + 1;
		
		if( oldPageNo > pageNo ) //rotate left to right
		{
			leftLeafNo = oldLeafNo - 2;
			rightLeafNo = oldLeafNo - 1;
		}
		
		if( leftLeafNo >= pagesMaterials.Length - 2 )
			leftLeafNo = pagesMaterials.Length - 2;
		if( rightLeafNo >= pagesMaterials.Length - 1 )
			rightLeafNo = pagesMaterials.Length - 1;
		
		leftLeafNo = leafNo - 1;
		rightLeafNo = leafNo;
		
		if( leftLeafNo < 0 )
		{
			var rightRenderer = rightPage.GetComponent<MeshRenderer>();
			rightRenderer.material = pagesMaterials[0];
		
			return;
		}
		
		if( pageNo < pagesMaterials.Length - 1 )
		{
			rightPage.SetActive( true );
			
			var rightRenderer = rightPage.GetComponent<MeshRenderer>();
			rightRenderer.material = pagesMaterials[rightLeafNo];
			return;
		}
		else if( pageNo == pagesMaterials.Length - 1 ) //last page
		{
			rightPage.SetActive( false );
			return;
		}
		else 
		{
			Debug.Log( "PageDisplayer::ShowRightPage :> Invalid page number " + pageNo.ToString() );
			return;
		}
	}

	void DisplayPages ()
	{
		Debug.Log( oldPageNo.ToString() + " n:" + pageNo.ToString() );

		if( !leftPage )
		{
			Debug.Log( "PageDisplayer::DisplayPages :> Left page not set" );
			return;
		}
		if( !rightPage )
		{
			Debug.Log( "PageDisplayer::DisplayPages :> Right page not set" );
			return;
		}
		if( !leftRotatingPage )
		{
			Debug.Log( "PageDisplayer::DisplayPages :> Left rotating page not set" );
			return;
		}
		if( !rightRotatingPage )
		{
			Debug.Log( "PageDisplayer::DisplayPages :> Right rotating page not set" );
			return;
		}

		var leftRotatingRenderer = leftRotatingPage.GetComponent<MeshRenderer>();
		var rightRotatingRenderer = rightRotatingPage.GetComponent<MeshRenderer>();

		int leafNo = pageNo*2;
		int oldLeafNo = oldPageNo*2;

		int leftLeafNo = oldLeafNo;
		int rightLeafNo = oldLeafNo + 1;

		if( oldPageNo > pageNo ) //rotate left to right
		{
			leftLeafNo = oldLeafNo - 2;
			rightLeafNo = oldLeafNo - 1;
		}

		if( leftLeafNo >= pagesMaterials.Length - 2 )
			leftLeafNo = pagesMaterials.Length - 2;
		if( rightLeafNo >= pagesMaterials.Length - 1 )
			rightLeafNo = pagesMaterials.Length - 1;

		leftRotatingRenderer.material = pagesMaterials[leftLeafNo];		
		rightRotatingRenderer.material = pagesMaterials[rightLeafNo];		
	}

	int oldPageNo;

	void HidePageRotator()
	{
		pageRotator.SetActive(false);
	}

	void HandleOnNextPageComplete()
	{
		ShowLeftPage();
		HidePageRotator();
	}

	void HandleOnPrevPageComplete()
	{
		ShowRightPage();
		HidePageRotator();
	}

	// this rotation describe start and stop positions (rotation) of 
	//	page rotator. -180.0F - is rotated to right maximally
	Vector3 rightRotation = new Vector3( 0.0F, -180.0F, 0.0F );
	Vector3 leftRotation = new Vector3( 0.0F, 0.0F, 0.0F );

	public void ShowNextPage()
	{
		pageNo++;
		if( pageNo == pagesMaterials.Length - 1 )
		{
			nextButton.interactable = false;
			prevButton.interactable = true;
		}

		if( pageNo > pagesMaterials.Length - 1 )
		{
			pageNo = pagesMaterials.Length - 1;
		}

		ShowRightPage();

		pageRotator.transform.eulerAngles = rightRotation;
		pageRotator.SetActive(true);
		LeanTween.rotateAroundLocal( pageRotator, Vector3.up, 180.0F, 1.0F).setOnComplete( HandleOnNextPageComplete );
	}

	public void ShowPrevPage()
	{
		pageNo--;
		if( pageNo == 0 )
		{
			prevButton.interactable = false;
			nextButton.interactable = true;
		}

		if( pageNo < 0 )
			pageNo = 0;

		ShowLeftPage();

		pageRotator.transform.eulerAngles = leftRotation;
		pageRotator.SetActive(true);
		LeanTween.rotateAroundLocal( pageRotator, Vector3.up, -180.0F, 1.0F).setOnComplete( HandleOnPrevPageComplete );
	}

	// Update is called once per frame
	void Update () {
		if( oldPageNo != pageNo )
		{
			DisplayPages();
			oldPageNo = pageNo;
		}
	}
}
