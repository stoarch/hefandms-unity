using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;
using System.Diagnostics;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public class tTouchData {
	public int m_x;
	public int m_y;
	public int m_ID;
	public int m_Time;
	public int m_Relativex;
	public int m_Relativey;
};


[StructLayout(LayoutKind.Sequential, Pack = 1)]
public class DeviceID : MonoBehaviour {

	public bool m_initialized = false;
	public struct IntelDeviceInfo
	{
		public uint GPUMaxFreq;
		public uint GPUMinFreq;
		public uint GTGeneration;
		public uint EUCount;
		public uint PackageTDP;
		public uint MaxFillRate;
	};

	const uint VALUE_NOT_DEFINED = 0xbaadf00d;

	IntelDeviceInfo IDInfo;

	[DllImport("IntelUnityPlugIn")]
	public static extern IntelDeviceInfo GetDeviceInfo();

	[DllImport("IntelUnityPlugIn")]
	public static extern int GetDeviceId();
	
	[DllImport("IntelUnityPlugIn")]
	public static extern bool IsDocked();
	
	[DllImport("IntelUnityPlugIn")]
	public static extern bool InSlateMode();
	
	[DllImport("IntelUnityPlugIn")]
	public static extern int GetBatteryMode();
	
	[DllImport("IntelUnityPlugIn")]
	public static extern bool InACMode();
	
	[DllImport("IntelUnityPlugIn")]
	public static extern int GetPercentBatteryLife();	

	[DllImport("IntelUnityPlugIn")]
	public static extern int GetTouchPointCount();

	[DllImport("IntelUnityPlugIn")]
	public static extern void GetTouchPoint(int i, tTouchData n);
	
	[DllImport("IntelUnityPlugIn", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
	public static extern int Initialise( string ProcessName );

	int deviceID = 0;
	public GUIText DeviceText;
	public GUIText DockedText;
	public GUIText SlateText;
	public GUIText GPUMaxText;
	public GUIText GPUMinText;
	public GUIText GTGenText;
	public GUIText EUText;
	public GUIText TDPText;
	public GUIText FillRateText;
	public GUIStyle GStyle;
	public GameObject Finger1;
	public GameObject Finger2;
	public GameObject Finger3;
	public GameObject Finger4;
	public GameObject Finger5;
	public StretchToScreenSize InfoPanel;
	public StretchToScreenSize SlidePanel;
	public GameObject TextGrouping;

	// Use this for initialization
	void Start () {
		
		m_initialized = false;
		GStyle.normal.textColor = Color.black;
		Finger1.SetActive(false);
		Finger2.SetActive(false);
		Finger3.SetActive(false);
		Finger4.SetActive(false);
		Finger5.SetActive(false);
	}
	
	void OnGUI() 
	{
		if(!m_initialized)
		{
#if DEBUG
			//UnityHiddenWindow
			//Unity - DeviceId.unity - DeviceID - PC, Mac & Linux Standalone*
			string str = "UnityHiddenWindow";
#else
			string str = "DeviceID";
#endif
			int retval = Initialise(str);
			deviceID = GetDeviceId();
			DeviceText.text = "0x"+deviceID.ToString ("X");
			SlateText.text = "AC Power";
			DockedText.text = "Percent Battery Life:";
			TDPText.text = "Undefined";
			GTGenText.text = "Undefined";
			EUText.text = "Undefined";
			FillRateText.text = "Undefined";
			GPUMaxText.text = "Undefined";
			GPUMinText.text = "Undefined";
		
			IDInfo = GetDeviceInfo();
			if( VALUE_NOT_DEFINED != IDInfo.GPUMaxFreq )
				GPUMaxText.text = IDInfo.GPUMaxFreq.ToString();
			if( VALUE_NOT_DEFINED != IDInfo.GPUMinFreq )
				GPUMinText.text = IDInfo.GPUMinFreq.ToString();
			if( VALUE_NOT_DEFINED != IDInfo.EUCount )
				EUText.text = IDInfo.EUCount.ToString();
			if( VALUE_NOT_DEFINED != IDInfo.GTGeneration )
				GTGenText.text = IDInfo.GTGeneration.ToString();
			if( VALUE_NOT_DEFINED != IDInfo.PackageTDP )
				TDPText.text = IDInfo.PackageTDP.ToString();
			if( VALUE_NOT_DEFINED != IDInfo.MaxFillRate )
				FillRateText.text = IDInfo.MaxFillRate.ToString();

			if(retval == 0)
				m_initialized = true;
		}

		if( m_initialized) {

			int NumTouch = GetTouchPointCount ();
			string Str;

			Finger1.SetActive(false);
			Finger2.SetActive(false);
			Finger3.SetActive(false);
			Finger4.SetActive(false);
			Finger5.SetActive(false);

			if( NumTouch == 0){
				InfoPanel.DoFadeIn();
				SlidePanel.DoSlideIn();
				TextGrouping.SetActive(true);
			}
			else {
				InfoPanel.DoFadeOut();
				SlidePanel.DoSlideOut();
				TextGrouping.SetActive(false);
			}

			//put some text on screen for each touch point
			for( int i = 0; i < NumTouch; i++){
				tTouchData TouchData = new tTouchData();
				GetTouchPoint( i, TouchData );
				TouchData.m_x /= 100;
				TouchData.m_y /= 100;
				Str = "XRelativePos: " + TouchData.m_Relativex.ToString() + " YRelativePos: " + TouchData.m_Relativey.ToString();				
				GUI.Label (new Rect (TouchData.m_Relativex, TouchData.m_Relativey,250,40), Str, GStyle);
				switch(i){
				case 0:
					Finger1.SetActive(true);
					Finger1.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(TouchData.m_Relativex, TouchData.m_Relativey, 0.0f));
					Finger1.transform.position = new Vector3( Finger1.transform.position.x, -Finger1.transform.position.y, 0.0f);
					break;
				case 1:
					Finger2.SetActive(true);
					Finger2.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(TouchData.m_Relativex, TouchData.m_Relativey, 0.0f));
					Finger2.transform.position = new Vector3( Finger2.transform.position.x, -Finger2.transform.position.y, 0.0f);
					break;
				case 2:
					Finger3.SetActive(true);
					Finger3.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(TouchData.m_Relativex, TouchData.m_Relativey, 0.0f));
					Finger3.transform.position = new Vector3( Finger3.transform.position.x, -Finger3.transform.position.y, 0.0f);
					break;
				case 3:
					Finger4.SetActive(true);
					Finger4.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(TouchData.m_Relativex, TouchData.m_Relativey, 0.0f));
					Finger4.transform.position = new Vector3( Finger4.transform.position.x, -Finger4.transform.position.y, 0.0f);
					break;
				case 4:
					Finger5.SetActive(true);
					Finger5.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(TouchData.m_Relativex, TouchData.m_Relativey, 0.0f));
					Finger5.transform.position = new Vector3( Finger5.transform.position.x, -Finger5.transform.position.y, 0.0f);
					break;
				}
			}
		}
		else{
			//Finger1.SetActive(true);
			//Finger1.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0.0f));
			//UnityEngine.Debug.Log(Finger1.transform.position);
			//Finger1.transform.position = new Vector3( Finger1.transform.position.x, Finger1.transform.position.y, 0.0f);
			//UnityEngine.Debug.Log(Finger1.transform.position);
			//Finger1.transform.position = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, cam.nearClipPlane));
			
		}
	}
	
	// Update is called once per frame
	void Update () {


		switch(GetBatteryMode()){
		case 0:
			SlateText.text = "Battery";
			break;
		case 1:
			SlateText.text = "AC Power";
			break;
		default:
			SlateText.text = "Undefined Power Mode";
			break;
		}
		DockedText.text = GetPercentBatteryLife().ToString();

	}
}
