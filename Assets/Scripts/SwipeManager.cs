﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class SwipeManager : MonoBehaviour
{
    public delegate void HandleSwipeEvent(EventArgs e, bool shortSwipe = true);
    public delegate void HandleFinger(EventArgs e, Vector2 position);
    public delegate void HandleFingerDrag(EventArgs e, Vector2 position, Vector2 delta, Vector2 totalDelta);

    public Animator swipeAnimator;
    public Slider putSlider;
    public Slider chanelSlider;

    [SerializeField]
    Animator rubberAnimator;

    public event HandleSwipeEvent OnSwipeUp;
    public event HandleSwipeEvent OnSwipeDown;
    public event HandleSwipeEvent OnSwipeLeft;
    public event HandleSwipeEvent OnSwipeRight;

    public event HandleFinger OnDown;
    public event HandleFinger OnUp;
    public event HandleFingerDrag OnDrag;

    private bool shortSwipe = true;

	protected virtual void OnEnable()
	{
		// Hook into the OnSwipe event
		Lean.LeanTouch.OnFingerSwipe += OnFingerSwipe;
        // Hook into the Down/Drag/Up event
        Lean.LeanTouch.OnFingerDown += OnFingerDown;
        Lean.LeanTouch.OnFingerDrag += OnFingerDrag;
        Lean.LeanTouch.OnFingerUp += OnFingerUp;
	}

    const float RIGHT_X_MARGIN = 50.0F;
    const float LEFT_X_MARGIN = -50.0F;
    const float UP_Y_MARGIN = 50.0F;
    const float DOWN_Y_MARGIN = -50.0F;


    private void OnFingerUp(Lean.LeanFinger finger)
    {
        var dx  = swipeAnimator.GetFloat("DeltaX");
        var dy = swipeAnimator.GetFloat("DeltaY");

        if (dx > RIGHT_X_MARGIN)
            SwipeRight();
        else if (dx < LEFT_X_MARGIN)
            SwipeLeft();

        if (dy > UP_Y_MARGIN)
            SwipeUp();
        else if (dy < DOWN_Y_MARGIN)
            SwipeDown();

        StartCoroutine(ResetDeltaX());
        StartCoroutine(ResetDeltaY());

        if (OnUp != null)
            OnUp(new EventArgs(), finger.ScreenPosition);
    }

    private IEnumerator ResetDeltaY()
    {
        var wait = new WaitForSeconds(0.01F);
        var dy = swipeAnimator.GetFloat("DeltaY");
        var startDy = dy;
        var time = 0.0F;
        if (startDy > 0)
        {
            while (dy > 0.1F)
            {
                time += 2 * Time.deltaTime;
                dy = Mathf.Lerp(startDy, 0.0F, time);
                swipeAnimator.SetFloat("DeltaY", dy);
                rubberAnimator.SetFloat("BottomDy", dy);
                yield return wait;
            }
            rubberAnimator.SetBool("DownRubberDragged", false);
        }
        else
        {
            while (dy < -0.1F)
            {
                time += 2 * Time.deltaTime;
                dy = Mathf.Lerp(startDy, 0.0F, time);
                swipeAnimator.SetFloat("DeltaY", dy);
                rubberAnimator.SetFloat("UpDy", dy);
                yield return wait;
            }
            rubberAnimator.SetBool("UpRubberDragged", false);
        }
    }

    private IEnumerator ResetDeltaX()
    {
        var wait = new WaitForSeconds(0.01F);
        var dx = swipeAnimator.GetFloat("DeltaX");
        var startDx = dx;
        var time = 0.0F;
        if (startDx > 0)
        {
            while (dx > 0.1F)
            {
                time += 2 * Time.deltaTime;
                dx = Mathf.Lerp(startDx, 0.0F, time);
                swipeAnimator.SetFloat("DeltaX", dx);
                rubberAnimator.SetFloat("LeftDx", dx);
                yield return wait;
            }
            rubberAnimator.SetBool("LeftRubberDragged", false);
        }
        else
        {
            while (dx < -0.1F)
            {
                time += 2 * Time.deltaTime;
                dx = Mathf.Lerp(startDx, 0.0F, time);
                swipeAnimator.SetFloat("DeltaX", dx);
                rubberAnimator.SetFloat("RightDx", dx);
                yield return wait;
            }
            rubberAnimator.SetBool("RightRubberDragged", false);
        }
    }

    const float VERTICAL_MARGIN = 5.0F;
    const float HORIZONTAL_MARGIN = 5.0F;

    float oldDx = 0.0F;
    float oldDy = 0.0F;

    private void OnFingerDrag(Lean.LeanFinger finger)
    {
        var dx = finger.TotalDeltaScreenPosition.x;
        var dy = finger.TotalDeltaScreenPosition.y;

        //preserve last direction (no jumping from vertical to horizontal)
        if( Mathf.Abs(oldDx) > Math.Abs(oldDy) )
        {
            oldDx = dx;
            oldDy = 0.0F;
            dy = 0.0F;
        }
        else if( Mathf.Abs(oldDx) < Mathf.Abs(oldDy))
        {
            oldDy = dy;
            oldDx = 0.0F;
            dx = 0.0F;
        }
        else
        {
            oldDx = dx;
            oldDy = dy;
        }

        if ( Mathf.Abs(dx) > Mathf.Abs(dy) )//horizontal drag
        {
            swipeAnimator.SetFloat("DeltaX", dx);
            swipeAnimator.SetFloat("DeltaY", 0.0F);

            if (dx > 0 && chanelSlider.value == 0)
            {
                rubberAnimator.SetBool("LeftRubberDragged", true);
                rubberAnimator.SetBool("RightRubberDragged", false);
                rubberAnimator.SetFloat("LeftDx", dx);
            }
            else if (dx < 0 && chanelSlider.value == chanelSlider.maxValue)
            {
                rubberAnimator.SetBool("RightRubberDragged", true);
                rubberAnimator.SetBool("LeftRubberDragged", false);
                rubberAnimator.SetFloat("RightDx", dx);
            }
            else
            {
                rubberAnimator.SetBool("RightRubberDragged", false);
                rubberAnimator.SetBool("LeftRubberDragged", false);
                rubberAnimator.SetFloat("LeftDx", 0.0F);
                rubberAnimator.SetFloat("RightDx", 0.0F);
            }
        }
        else if ( Mathf.Abs(dx) < Mathf.Abs(dy) ) //vertical drag
        {
            swipeAnimator.SetFloat("DeltaX", 0.0F);
            swipeAnimator.SetFloat("DeltaY", dy);

            if (dy < 0 && putSlider.value == 0)
            {
                rubberAnimator.SetBool("UpRubberDragged", true);
                rubberAnimator.SetBool("DownRubberDragged", false);
                rubberAnimator.SetFloat("UpDy", dy);
            }
            else if (dy > 0 && putSlider.value == putSlider.maxValue)
            {
                rubberAnimator.SetBool("DownRubberDragged", true);
                rubberAnimator.SetBool("UpRubberDragged", false);
                rubberAnimator.SetFloat("BottomDy", dy);
            }
            else
            {
                rubberAnimator.SetBool("UpRubberDragged", false);
                rubberAnimator.SetBool("DownRubberDragged", false);
                rubberAnimator.SetFloat("UpDx", 0.0F);
                rubberAnimator.SetFloat("BottomDx", 0.0F);
            }
        }
        else
        {
            rubberAnimator.SetBool("UpRubberDragged", false);
            rubberAnimator.SetBool("DownRubberDragged", false);
            rubberAnimator.SetFloat("UpDx", 0.0F);
            rubberAnimator.SetFloat("BottomDx", 0.0F);
            rubberAnimator.SetBool("RightRubberDragged", false);
            rubberAnimator.SetBool("LeftRubberDragged", false);
            rubberAnimator.SetFloat("LeftDx", 0.0F);
            rubberAnimator.SetFloat("RightDx", 0.0F);
        }

        if (OnDrag != null)
            OnDrag(new EventArgs(), finger.ScreenPosition, finger.DeltaScreenPosition, finger.TotalDeltaScreenPosition);

    }

    private void OnFingerDown(Lean.LeanFinger finger)
    {
        oldDx = 0.0F;
        oldDy = 0.0F;

        StopCoroutine("ResetDeltaX");
        StopCoroutine("ResetDeltaY");

        if (OnDown != null)
            OnDown(new EventArgs(), finger.ScreenPosition);
    }
	
	protected virtual void OnDisable()
	{
		// Unhook into the OnSwipe event
		Lean.LeanTouch.OnFingerSwipe -= OnFingerSwipe;
        // UnHook into the Down/Drag/Up event
        Lean.LeanTouch.OnFingerDown -= OnFingerDown;
        Lean.LeanTouch.OnFingerDrag -= OnFingerDrag;
        Lean.LeanTouch.OnFingerUp -= OnFingerUp;
	}

    private float swipeMargin = Screen.height/2;//after that it will be long swipe

	public void OnFingerSwipe(Lean.LeanFinger finger)
	{
        var isSwipeY = Mathf.Abs(finger.SwipeDelta.x) < Mathf.Abs(finger.SwipeDelta.y);
        var isSwipeX = Mathf.Abs(finger.SwipeDelta.x) > Mathf.Abs(finger.SwipeDelta.y);

        shortSwipe = true;


        if (isSwipeX)
        {
            if (Mathf.Abs(finger.SwipeDelta.x) > swipeMargin)
                shortSwipe = false;

            if (finger.SwipeDelta.x < 0) 
                SwipeLeft();
            else if (finger.SwipeDelta.x > 0) 
                SwipeRight();
        }

        if (isSwipeY)
        {
            if (Mathf.Abs(finger.SwipeDelta.y) > swipeMargin)
                shortSwipe = false;

            if (finger.SwipeDelta.y > 0)
                SwipeUp();
            else if (finger.SwipeDelta.y < 0)
                SwipeDown();
        }

	}

    private void SwipeDown()
    {
        if( putSlider.value == 0 )
        {
            return;
        }

        if (shortSwipe)
        {
            swipeAnimator.SetInteger("RepeatCount", 0);
            swipeAnimator.SetFloat("UpDownAnimationSpeed", 1.0F);
            putSlider.value = putSlider.value - 1;
        }
        else 
        {
            if (putSlider.value > 3)
            {
                swipeAnimator.SetInteger("RepeatCount", 2);
                swipeAnimator.SetFloat("UpDownAnimationSpeed", 3.0F);
                putSlider.value = putSlider.value - 3;
            }
            else
            {
                swipeAnimator.SetInteger("RepeatCount", (int)(putSlider.value - 1));
                swipeAnimator.SetFloat("UpDownAnimationSpeed", 1.5F);
                putSlider.value = 0;
            }
        }
        
        if (OnSwipeDown != null)
            OnSwipeDown(new EventArgs(), shortSwipe);

        swipeAnimator.SetTrigger("SwipeDown");
    }

    private void SwipeUp()
    {
        if( putSlider.value == putSlider.maxValue )
        {
            return;
        }


        if (shortSwipe)
        {
            swipeAnimator.SetInteger("RepeatCount", 0);
            swipeAnimator.SetFloat("UpDownAnimationSpeed", 1.0F);
            putSlider.value = putSlider.value + 1;
        }
        else
        {
            if (putSlider.value < putSlider.maxValue - 3)
            {
                swipeAnimator.SetInteger("RepeatCount", 2);
                swipeAnimator.SetFloat("UpDownAnimationSpeed", 3.0F);
                putSlider.value = putSlider.value + 3;
            }
            else
            {
                swipeAnimator.SetInteger("RepeatCount", (int)(putSlider.maxValue - putSlider.value - 1));
                swipeAnimator.SetFloat("UpDownAnimationSpeed", 1.5F);
                putSlider.value = putSlider.maxValue;
            }
        }

        if (OnSwipeUp != null)
            OnSwipeUp(new EventArgs(), shortSwipe);

        swipeAnimator.SetTrigger("SwipeUp");
    }

    private void SwipeRight()
    {
        if( chanelSlider.value <= 0 )
        {
            return;
        }

        chanelSlider.value = chanelSlider.value - 1;

        if (OnSwipeRight != null)
            OnSwipeRight(new EventArgs(), shortSwipe);

        swipeAnimator.SetTrigger("SwipeRight");

    }

    private void SwipeLeft()
    {
        if( chanelSlider.value >= chanelSlider.maxValue  )
        {
            return;
        }

        chanelSlider.value = chanelSlider.value + 1;

        if (OnSwipeLeft != null)
            OnSwipeLeft(new EventArgs(), shortSwipe);

        swipeAnimator.SetTrigger("SwipeLeft");
    }
}


