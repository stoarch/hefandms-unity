﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(LineRenderer))]
public class ThermometerIndicator : MonoBehaviour {

    public float minValue = 0;
    public float maxValue = 100;

    [SerializeField]
    private float mvalue = 20;
    public float Value
    {
        get { return mvalue; }
        set { SetValue(value);}
    }
    

    public Vector3[] linePositions;
    public float speed = 1f;
    public Text valueViewer;

    private LineRenderer line;
    private float oldValue = 20F;

    public void SetValue( float value )
    {
        mvalue = value;
        valueViewer.text = value.ToString("000");
    }

	void Start () 
    {
        line = gameObject.GetComponent<LineRenderer>();

        oldValue = mvalue;
        SetLineSizeForValue();
	}

    private void SetLineSizeForValue()
    {
        line.SetPosition(1, 
            new Vector3( 
                linePositions[0].x,  
                Mathf.Lerp( 
                    linePositions[0].y, 
                    linePositions[1].y, 
                    (oldValue - minValue)/(maxValue - minValue)), 
                linePositions[0].z
            )
        );
    }

    const float EPS = 1e-1F;
	
	void Update () 
    {
        if (oldValue < minValue)
            return;

        if (oldValue > maxValue)
            return;

        if (Mathf.Abs(oldValue - mvalue) < EPS )
            return;

        oldValue += speed*Time.deltaTime*(mvalue - oldValue);
        DebugPanel.Log("oldValue", "value", oldValue );

        SetLineSizeForValue();

	}
}
