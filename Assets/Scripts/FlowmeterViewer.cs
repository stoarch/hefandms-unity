﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FlowmeterViewer : MonoBehaviour {

    [SerializeField]
    private float mValue = 0.0F;
    public Text valueViewer;

    public float Value
    {
        get
        {
            return mValue;
        }
        set
        {
            mValue = value;
            UpdateView();
        }
    }

    void UpdateView()
    {
        valueViewer.text = mValue.ToString("0000.0");
    }
}
