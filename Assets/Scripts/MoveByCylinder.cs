﻿using UnityEngine;
using System.Collections;

public class MoveByCylinder : MonoBehaviour {

	public GameObject origin;

	private Vector3 startPos;
	private float dx;

	// Use this for initialization
	void Start () {
		startPos = transform.position;

		if( !origin )
			return;

		dx = Mathf.Abs( startPos.x - origin.transform.position.x );
	}
	
	// Update is called once per frame
	void Update () {
		if( Input.GetMouseButtonDown(0)) 
		{
			var mousePos = Camera.main.ScreenToWorldPoint( new Vector3( Input.mousePosition.x, Input.mousePosition.y, 0.0F ) );
			float dxCur = Mathf.Abs( origin.transform.position.x - mousePos.x );

			mousePos.z = startPos.z - Mathf.Sqrt( dx*dx - dxCur*dxCur );

			transform.position = mousePos;
		}	
	}
}
