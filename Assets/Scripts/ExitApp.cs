﻿using UnityEngine;
using System.Collections;

public class ExitApp : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	public void QuitApp(){
		Application.Quit();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("escape"))
			Application.Quit();
	}
}
