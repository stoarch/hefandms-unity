﻿using UnityEngine;
using System.Collections;

public class SwipeAnimationHandler : MonoBehaviour {

    public ChanelValueReceiver chanelValsReceiver;

    private Animator animator;
    private SwipeChanelsLeftState swipeLeftState;
    private SwipeRightChanelsState swipeRightState;
    private SwipeUpChanelState swipeUpState;
    private SwipeDownAnimState swipeDownState;

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        swipeLeftState = animator.GetBehaviour<SwipeChanelsLeftState>();
        swipeRightState = animator.GetBehaviour<SwipeRightChanelsState>();
        swipeUpState = animator.GetBehaviour<SwipeUpChanelState>();
        swipeDownState = animator.GetBehaviour<SwipeDownAnimState>();

        swipeLeftState.animHandler = this;
        swipeRightState.animHandler = this;
        swipeUpState.animHandler = this;
        swipeDownState.animHandler = this;
    }

    public void HandleDownSwipeEnd()
    {
        chanelValsReceiver.EqualizeValues();
        DecrementRepeat();//we only fast moving in up/down direction, chanels count is very short
    }

    private void DecrementRepeat()
    {
        var repeat = animator.GetInteger("RepeatCount");
        if (repeat > 0)
            animator.SetInteger("RepeatCount", repeat - 1);
    }

    public void HandleUpSwipeEnd()
    {
        chanelValsReceiver.EqualizeValues();
        DecrementRepeat();
    }

    public void HandleRightSwipeEnd()
    {
        chanelValsReceiver.EqualizeValues();
    }

    public void HandleLeftSwipeEnd()
    {
        chanelValsReceiver.EqualizeValues();
    }

}
