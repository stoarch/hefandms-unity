﻿using UnityEngine;
using System.Collections;

public class QuitManager : MonoBehaviour {

    public void Quit()
    {
        Application.Quit();
    }
}
