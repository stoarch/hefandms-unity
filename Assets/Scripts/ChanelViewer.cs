﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct ChanelViewer
{
    public Text putNameText;
    public Text chanelNameText;
    public ManometerViewer manometer;
    public ThermometerIndicator thermometer;
    public FlowmeterViewer flowmeter;
    public GameObject container; 
}
