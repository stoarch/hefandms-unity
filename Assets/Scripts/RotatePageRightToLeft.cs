﻿using UnityEngine;
using System.Collections;

public class RotatePageRightToLeft : MonoBehaviour {

	public GameObject pageRotator;
	public float angleStep = 5.0F;

	public void StartRotation()
	{
		pageRotator.SetActive( true );
		StartCoroutine( "Rotate" );
	}

	IEnumerator Rotate()
	{
		if( !pageRotator )
		{
			Debug.Log( "RotatePageRightToLeft::Rotate :> Page rotator does not set" );
			yield break;
		}


		float angle = 180.0F;

		while( angle < 360.0F )
		{
			pageRotator.transform.eulerAngles = new Vector3( 0.0F, angle, 0.0F );
			angle += angleStep;
			yield return new WaitForSeconds( 0.01F );
		}

		pageRotator.SetActive( false );
	}
}
