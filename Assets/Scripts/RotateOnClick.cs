﻿using UnityEngine;
using System.Collections;

public class RotateOnClick : MonoBehaviour {

	public float angularSpeed = 3.0F; //angles in second

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseUp()
	{
		StartCoroutine( "Rotate" );
	}

	IEnumerator Rotate()
	{
		float angle = 0.0F;
		while( angle < 180.0F )
		{
			gameObject.transform.localEulerAngles = new Vector3( 0.0F, angle, 0.0F );
			angle += angularSpeed;
			yield return new WaitForSeconds( 0.1F );
		}
	}
}
