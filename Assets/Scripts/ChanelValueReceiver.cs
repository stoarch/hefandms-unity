﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Scripts;
using AVSGame.Mnemoscheme.Put;


public class ChanelValueReceiver : MonoBehaviour
{
    public int putNo;
    public int chanelNo;

    public ChanelViewer currentChanel;
    public ChanelViewer nextChanel;

    private ChanelViewer activeChanel;

    public Slider chanelSlider;

    public bool isDemoMode = false;

    private bool cached = false;


    private Dictionary<int, PutData> putCache = new Dictionary<int,PutData>();

    void Start()
    {
        activeChanel = currentChanel;
        QueryData();
    }

    public void SetDemoMode()
    {
        isDemoMode = true;
    }

    public void ResetDemoMode()
    {
        isDemoMode = false;
    }

    private int oldPutNo = 0;

    public void QueryData()
    {
        if(isDemoMode)
        {
            SetTemperature(Random.Range(0,100));
            SetPressure(Random.Range(0,7));
            SetFlowrate(Random.Range(0,3500));

            SetPutName("Put" + putNo);
            SetChanelName("Chanel" + chanelNo);
            if( oldPutNo != putNo )
            {
                SetChanelCount(Random.Range(2, 6));
                oldPutNo = putNo;
            }
            return;
        }

        string url = WebServiceParams.ServiceURL + string.Format("puts/{0}/chanels/{1}/values", putNo, chanelNo);

        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www));
    }

    float temp_val;
    float press_val;
    float flow_val;

    string temp_date;
    string press_date;
    string flow_date;

    string putName;
    string chanelName;
    int chanelCount;

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        if (www.error == null)
        {
            JSONObject value = new JSONObject(www.text);

            var put = value.GetField("put");
            var chanel = value.GetField("chanel");
            var temperature = value.GetField("temperature");
            var pressure = value.GetField("pressure");
            var flowrate = value.GetField("flowrate");

            temp_val = 0.0F;
            press_val = 0.0F;
            flow_val = 0.0F;

            if (temperature != null)
            {
                var temp_field = temperature.GetField("value");
                if (temp_field != null)
                {
                    temp_val = temp_field.asFloat;
                    temp_date = temperature.GetField("date_query").asString;
                }
            }

            if (pressure != null)
            {
                var press_field = pressure.GetField("value");
                if (press_field != null)
                {
                    press_val = press_field.asFloat;
                    press_date = pressure.GetField("date_query").asString;
                }
            }

            if (flowrate != null)
            {
                var flow_field = flowrate.GetField("value");
                if (flow_field != null)
                {
                    flow_val = flow_field.asFloat;
                    flow_date = flowrate.GetField("date_query").asString;
                }
            }


            putName =put.GetField("caption").asString + "--" + put.GetField("name").asString;
            chanelName = chanel.GetField("caption").asString;
            chanelCount = (int)put.GetField("chanel_count").asInteger;

            SetValues();
        }
        else
        {
            Debug.LogError("WWW error:" + www.error);
        }
    }

    private void SetValues()
    {
        SetTemperature(temp_val);
        SetPressure(press_val);
        SetFlowrate(flow_val);

        SetTemperatureDate(temp_date);
        SetPressureDate(press_date);
        SetFlowrateDate(flow_date);

        SetPutName(putName);
        SetChanelName(chanelName);
        SetChanelCount(chanelCount);
    }

    private void SetChanelCount(int value)
    {
        if (!chanelSlider)
            return;

        if (chanelNo >= value)
            chanelNo = value - 1;
        if( chanelNo < 0 )
            chanelNo = 0;

        chanelSlider.minValue = 0;
        chanelSlider.maxValue = value - 1;
        chanelSlider.value = chanelNo;
        chanelCount = value;
    }

    private void SetFlowrateDate(string flow_date)
    {
    }

    private void SetPressureDate(string val)
    {
        if (!activeChanel.manometer)
            return;

        activeChanel.manometer.DateString = val;
    }

    private void SetTemperatureDate(string val)
    {
    }

    private void SetFlowrate(float val)
    {
        if (!activeChanel.flowmeter)
            return;

        activeChanel.flowmeter.Value = val;
    }

    private void SetPressure(float val)
    {
        if (!activeChanel.manometer)
            return;

        activeChanel.manometer.Value = val;
    }

    private void SetTemperature(float val)
    {
        if (!activeChanel.thermometer)
            return;

        activeChanel.thermometer.Value = val;
    }

    private void SetChanelName(string name)
    {
        if (activeChanel.chanelNameText)
        {
            activeChanel.chanelNameText.text = name;
        }
    }

    private void SetPutName(string name)
    {
        if (activeChanel.putNameText)
        {
            activeChanel.putNameText.text = putNo.ToString() + " > " +  name;
        }
    }

    //access data (and print it)
    void accessData(JSONObject obj)
    {
        switch (obj.type)
        {
            case JSONObject.Type.OBJECT:
                Debug.Log(obj.type.ToString());
                for (int i = 0; i < obj.objects.Count; i++)
                {
                    string key = (string)obj.keys[i];
                    JSONObject j = (JSONObject)obj.objects[i];
                    Debug.Log(key);
                    accessData(j);
                }
                break;
            case JSONObject.Type.ARRAY:
                foreach (JSONObject j in obj.objects)
                {
                    accessData(j);
                }
                break;
            case JSONObject.Type.STRING:
                Debug.Log(obj.asString);
                break;
            case JSONObject.Type.NUMBER:
                Debug.Log(obj.asSingle);
                break;
            case JSONObject.Type.BOOL:
                Debug.Log(obj.asBoolean);
                break;
            case JSONObject.Type.NULL:
                Debug.Log("NULL");
                break;

        }
    }

    internal void Reload()
    {
        cached = false;
        QueryData();
    }

    internal void SwapValues()
    {
        var temp = nextChanel;
        nextChanel = currentChanel;
        currentChanel = temp;
        activeChanel = currentChanel;

        SetValues();
    }

    internal void EqualizeValues()
    {
        activeChanel = nextChanel;
        SetValues();
        activeChanel = currentChanel;
        SetValues();
    }

    internal void QueryNextData()
    {
        activeChanel = nextChanel;
        QueryData();
    }
}
