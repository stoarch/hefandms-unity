﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AVSGame.Mnemoscheme.Put
{
    public enum ValueKind
    {
        Unknown,
        Temperature,
        Pressure,
        Flowrate
    }


    public struct ValueData
    {
        public int key;
        public ValueKind kind;
        public float value;
    }

    public struct ChanelData
    {
        public const int MAX_VALUES = 3;

        public int key;
        public string caption;
        public ValueData[] values;
        public DateTime dateReceived;
        public DateTime dateCached;
    }

    public struct PutData
    {
        public int key;
        public DateTime dateReceived;
        public DateTime dateCached;
        public string caption;
        public string name;

        public List<ChanelData> chanelValues;
    }
}
