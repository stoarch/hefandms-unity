﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using UnityEngine.UI;

public class PutCountReceiver : MonoBehaviour {

    public Slider putSlider;
    public bool isDemoMode = false;

	void Start () {
        QueryData();
	}

    public void SetDemoMode()
    {
        isDemoMode = true;
    }

    public void ResetDemoMode()
    {
        isDemoMode = false;
    }

    public void QueryData()
    {
        if( isDemoMode )
        {
            putSlider.maxValue = 7;
            return;
        }

        string url = WebServiceParams.ServiceURL + "puts?countOnly=true";

        WWW www = new WWW(url);
        Debug.Log("Querying put count...");
        StartCoroutine(WaitForRequest(www));
    }
	
    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        if( www.error == null )
        {
            Debug.Log("WWW ok: " + www.text);

            JSONObject value = new JSONObject(www.text);

            var put_count = value.GetField("put_count").asInteger;

            Debug.LogFormat("Put count is {0}", put_count);

            putSlider.minValue = 0;
            putSlider.maxValue = put_count - 1;
        }
        else
        {
            Debug.LogError("WWW error:" + www.error);
        }
    }
}
