﻿using UnityEngine;
using System.Collections;

public class ManometerIndicator : MonoBehaviour {

    [SerializeField]
    private float mvalue;

    public float Value
    {
        get { return mvalue;  }
        set { mvalue = value; }
    }
    public float minValue = 0;
    public float maxValue = 6;

    public float[] betweenValueAngles = new float[]{40F, 0F, -45F, -90F, -135F, -180F, -225F};

    public float minAngle;
    public float maxAngle;

    public float curAngle;
    public float stepRotationAngle = 5;

    public float gizmoLength = 1.0F;

    const int EPS = 1;

    public void SetValue( float newValue )
    {
        Value = newValue;
        curAngle = FindAngleFor(Value);
    }

    private float FindAngleFor(float value)
    {
        var iVal = (int)(value - minValue) + 1;

        if( iVal <= (int)minValue )
            return betweenValueAngles[0];

        if (iVal > (int)maxValue)
            return betweenValueAngles[(int)maxValue];

        var prevIVal = iVal - 1;
        var prevAngle = betweenValueAngles[prevIVal];

        var angle = betweenValueAngles[iVal];

        var newAngle = prevAngle + Mathf.Lerp(0, angle - prevAngle, (value - prevIVal) / (iVal - prevIVal ));

        return newAngle;
    }

    void Update()
    {

        var angle = transform.rotation.eulerAngles.z;

        if (angle > 180)
            angle -= 360;

        if ((angle < 180) && (angle > 90))
            angle -= 360;

        float diffVelocity = 1f;

        if (Mathf.Abs(angle - curAngle) <= 1)
        {
            if (curAngle > 0)
            {
                diffVelocity = Mathf.Abs(curAngle - angle) / curAngle;
            }
            else
            {
                diffVelocity = Mathf.Abs(curAngle - angle) / Mathf.Abs(angle);
            }
        }

        if (Mathf.Abs(angle - curAngle) < EPS)
            return;

        if( angle > curAngle - EPS )
            transform.Rotate(0, 0, -stepRotationAngle * Time.deltaTime * diffVelocity);
        else if ( angle < curAngle + EPS )
            transform.Rotate(0, 0, stepRotationAngle * Time.deltaTime * diffVelocity);
    }
}
