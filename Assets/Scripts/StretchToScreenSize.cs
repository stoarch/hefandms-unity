using UnityEngine;
using System.Collections;

public class StretchToScreenSize : MonoBehaviour {
	
	public float distance = 0;
	public float goDepth = 0;
	public float xPercent = 1;
	Vector3 v3ViewPort;
	Vector3 v3BottomLeft;
	Vector3 v3TopRight;
	bool DoFade = false;
	float TargetAlpha = 1;
	float lerptime;
	float TotalFadeTime = 0.1f;
	float FadeTime = 0;
	public bool IsSlideable = false;
	public bool IsFadeable = false;
	bool DoSlide;
	float delta = 0;
	float SlideAmount = 0;
	bool isRight = true;

	void Start () {
		distance -= (goDepth*0.5f);
		
		v3ViewPort.Set(1,1,distance);
		v3BottomLeft = Camera.main.ViewportToWorldPoint(v3ViewPort);
		v3ViewPort.Set(0,0,distance);
		v3TopRight = Camera.main.ViewportToWorldPoint(v3ViewPort);
		delta = (v3BottomLeft.x-v3TopRight.x) * xPercent;
		transform.localScale = new Vector3(delta,v3BottomLeft.y-v3TopRight.y,goDepth);
		if(xPercent != 1.0f){
			delta = (v3BottomLeft.x-v3TopRight.x) * (1.0f - xPercent);
			transform.position -= new Vector3(delta/2, 0, 0);
		}
	}

	
	// Update is called once per frame
	void Update () {

//		v3ViewPort.Set(1,1,distance);
//		v3BottomLeft = Camera.main.ViewportToWorldPoint(v3ViewPort);
//		v3ViewPort.Set(0,0,distance);
//		v3TopRight = Camera.main.ViewportToWorldPoint(v3ViewPort);
//			
//		transform.localScale = new Vector3((v3BottomLeft.x-v3TopRight.x) * xPercent,v3BottomLeft.y-v3TopRight.y,goDepth);
		if(DoFade){
			Fade();
		}
		if(DoSlide){
			Slide();
		}

	}

	void Slide(){
		transform.position -= new Vector3(SlideAmount, 0, 0);
		DoSlide = false;
	}

	public void DoSlideIn(){
		if(!DoSlide && IsSlideable && !isRight){
			DoSlide = true;
			SlideAmount = delta * 0.81f;
			isRight = true;
		}
	}

	public void DoSlideOut(){
		if(!DoSlide && IsSlideable && isRight){
			DoSlide = true;
			SlideAmount = -delta * 0.81f;
			isRight = false;
		}
	}
	

	public void DoFadeIn(){
		if(DoFade == false && IsFadeable) {
			DoFade = true;
			lerptime = 0;
			FadeTime = 0;
			TargetAlpha = 1.0f;
		}
	}

	public void DoFadeOut(){
		if(DoFade == false && IsFadeable) {
			DoFade = true;
			lerptime = 0;
			FadeTime = 0;
			TargetAlpha = 0.0f;
		}
	}


	void Fade()
	{
		FadeTime += Time.deltaTime;
		lerptime += (Time.deltaTime / TotalFadeTime) ;
		Color originalColor = GetComponent<Renderer>().material.color;
		float currentAlpha = Mathf.Lerp(originalColor.a, TargetAlpha, lerptime);
		GetComponent<Renderer>().material.color = new Color(originalColor.r, originalColor.g, originalColor.b, currentAlpha);

		//are we done?
		if(FadeTime >= TotalFadeTime)
			DoFade = false;
	}
}
