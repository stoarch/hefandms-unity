using UnityEngine;
using System.Collections;

public class Anchor : MonoBehaviour {
	
	public float XAnchor;
	public float YAnchor;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		this.GetComponent<GUIText>().pixelOffset = new Vector2( Screen.width * XAnchor, Screen.height * YAnchor);
	}
}
