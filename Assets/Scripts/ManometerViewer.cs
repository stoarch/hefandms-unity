﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ManometerViewer : MonoBehaviour {

    public Text valueView;
    public ManometerIndicator arrow;
    public Text dateView;

    [SerializeField]
    private float mvalue;

    public float Value{get{return mvalue;}set{SetValue(value);}}

    [SerializeField]
    private string mdateString;

    public string DateString { get { return mdateString; } set { SetDateString(value); } }

    private void SetDateString(string value)
    {
        mdateString = value;

        dateView.text = value;
    }

    public void SetValue( float newValue )
    {
        mvalue = newValue;

        valueView.text = mvalue.ToString("0.00");
        arrow.SetValue(mvalue);
    }
}
