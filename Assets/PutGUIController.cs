﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

/// <summary>
/// Handle GUI changes and call chanel manager to load new data
/// </summary>
public class PutGUIController : MonoBehaviour {

    public SwipeManager swiper;
    public ChanelValueReceiver chanelValsReceiver;
    public int chanelNo;
    public int putNo;

    [SerializeField]
    ChanelViewer leftChanel;
    [SerializeField]
    ChanelViewer rightChanel;
    [SerializeField]
    ChanelViewer upChanel;
    [SerializeField]
    ChanelViewer downChanel;
    [SerializeField]
    ChanelViewer centerChanel;
    [SerializeField]
    Slider putSlider;

    [SerializeField]
    Animator topSwipeHitAnim;

    int oldPutNo = 0;

    void Start()
    {
        swiper.OnSwipeUp += HandleSwipeUp;
        swiper.OnSwipeDown += HandleSwipeDown;
        swiper.OnSwipeLeft += HandleSwipeLeft;
        swiper.OnSwipeRight += HandleSwipeRight;

        swiper.OnDown += swiper_OnDown;
        swiper.OnUp += swiper_OnUp;
        swiper.OnDrag += swiper_OnDrag;
    }

    bool isPressedDown = false;

    void swiper_OnDrag(EventArgs e, Vector2 position, Vector2 delta, Vector2 totalDelta)
    {
        //TODO: set position for animation based on delta
    }

    void swiper_OnUp(EventArgs e, Vector2 position)
    {
        isPressedDown = false;

    }

    void swiper_OnDown(EventArgs e, Vector2 position)
    {
        isPressedDown = true;
    }

    private void HandleSwipeRight(EventArgs e, bool shortSwipe)
    {
        chanelNo--;
        chanelValsReceiver.nextChanel = leftChanel;
        chanelValsReceiver.currentChanel = centerChanel;
        QueryNextData();
    }

    private void HandleSwipeLeft(EventArgs e, bool shortSwipe)
    {
        chanelNo++;
        chanelValsReceiver.nextChanel = rightChanel;
        chanelValsReceiver.currentChanel = centerChanel;
        QueryNextData();
    }

    private void QueryNextData()
    {
        chanelValsReceiver.chanelNo = chanelNo;
        chanelValsReceiver.putNo = putNo;
        chanelValsReceiver.QueryNextData();
    }

    private void HandleSwipeDown(EventArgs e, bool shortSwipe)
    {
        oldPutNo = putNo;
        putNo = (int)(putSlider.value);
        chanelNo = 0;

        if (oldPutNo != putNo)
        {
            chanelValsReceiver.nextChanel = upChanel;
            chanelValsReceiver.currentChanel = centerChanel;
            QueryNextData();
        }
    }

    private void HandleSwipeUp(EventArgs e, bool shortSwipe)
    {
        oldPutNo = putNo;
        putNo = (int)(putSlider.value);

        chanelNo = 0;
        if (oldPutNo != putNo)
        {
            chanelValsReceiver.nextChanel = downChanel;
            chanelValsReceiver.currentChanel = centerChanel;
            QueryNextData();
        }
    }

    private void QueryData()
    {
        Debug.LogFormat("Put {0} Chanel {1}", putNo, chanelNo);
        chanelValsReceiver.chanelNo = chanelNo;
        chanelValsReceiver.putNo = putNo;
        chanelValsReceiver.QueryData();
    }

}
