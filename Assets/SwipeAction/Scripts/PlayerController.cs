using UnityEngine;

using System.Collections;

enum ActiveSkillMode
{
    Melee,
    Ranged
};

public enum OrientationType
{
    Portrait,
    Landscape
};

internal class PlayerMover
{
    internal Transform transform;
    internal Vector3 move_delta;
    internal float PlayerSpeed;
    internal GameObject gameObject;
    internal TouchSwipeController toucher;
    internal bool active_movement_toggle;

    internal void MoveIn2D()
    {
        if (Camera.main.orthographic)
        {
            HandleMovementKeys();

            transform.position = transform.position + move_delta;
        }

    }

    internal void MoveOrStopPhysically()
    {
        if (active_movement_toggle)
        {
            SetLowDrag();

            UpdatePositionThroughInputs();
        }
        else
        {
            SetStopingDrag();
        }
    }

    private void SetStopingDrag()
    {
        if (transform.GetComponent<Rigidbody>().drag != 10)
            transform.GetComponent<Rigidbody>().drag = 10;
    }

    private void SetLowDrag()
    {
        if (transform.GetComponent<Rigidbody>().drag != 1)
            transform.GetComponent<Rigidbody>().drag = 1;
    }


    internal void HandleMovementKeys()
    {
        if (Input.GetKey(KeyCode.W))
        {
            SetAnimationToMoveUp();
            MoveUp();
        }
        else if (Input.GetKey(KeyCode.S))
        {
            SetAnimationToIdle();
            MoveDown();
        }
        else
            move_delta.z = 0;

        if (Input.GetKey(KeyCode.D))
        {
            SetAnimationToMoveRight();
            MoveRight();
        }
        else if (Input.GetKey(KeyCode.A))
        {
            SetAnimationToMoveLeft();
            MoveLeft();
        }
        else
            move_delta.x = 0;
    }

    private void MoveLeft()
    {
        move_delta.x = -PlayerSpeed * Time.deltaTime;
    }

    private void SetAnimationToMoveLeft()
    {
        transform.GetComponent<AnimationBehavior>().SendMessage("setMovementState", MovementState.MoveLeft);
    }

    private void MoveRight()
    {
        move_delta.x = PlayerSpeed * Time.deltaTime;
    }

    private void SetAnimationToMoveRight()
    {
        transform.GetComponent<AnimationBehavior>().SendMessage("setMovementState", MovementState.MoveRight);
    }

    private void MoveDown()
    {
        move_delta.z = -PlayerSpeed * Time.deltaTime;
    }

    private void MoveUp()
    {
        move_delta.z = PlayerSpeed * Time.deltaTime;
    }

    private void SetAnimationToIdle()
    {
        transform.GetComponent<AnimationBehavior>().SendMessage("setMovementState", MovementState.Idle);
    }

    private void SetAnimationToMoveUp()
    {
        transform.GetComponent<AnimationBehavior>().SendMessage("setMovementState", MovementState.MoveUp);
    }

    // Updates the Movement Direction based on position pressed in the Controller
    internal void UpdatePositionThroughInputs()
    {
        var active_angle = toucher.FindActiveAngle();
        if (!Camera.main.orthographic)
            transform.rotation = Quaternion.Euler(0, -(Mathf.Rad2Deg * active_angle) + 90, 0);

        move_delta.z = (PlayerSpeed * Mathf.Sin(active_angle)) * Time.deltaTime;
        move_delta.x = (PlayerSpeed * Mathf.Cos(active_angle)) * Time.deltaTime;

        if (Camera.main.orthographic)
            transform.position += move_delta;
        else
            transform.GetComponent<Rigidbody>().AddForce(PlayerSpeed * move_delta.normalized, ForceMode.Force);

        if (gameObject.GetComponent<AnimationBehavior>() != null)
            ChangeMovementStateFromGridDir(move_delta.normalized);

        move_delta = Vector3.zero;
        active_movement_toggle = false;
    }

    // Changes the Frame of the MainPlayer Texture based off the normalized direction values
    protected void ChangeMovementStateFromGridDir(Vector3 direction)
    {
        if (direction.x > .5)
            transform.GetComponent<AnimationBehavior>().SendMessage("setMovementState", MovementState.MoveRight);
        else if (direction.x < -.5)
            transform.GetComponent<AnimationBehavior>().SendMessage("setMovementState", MovementState.MoveLeft);
        else if (direction.z > 0)
            transform.GetComponent<AnimationBehavior>().SendMessage("setMovementState", MovementState.MoveUp);
        else if (direction.z < 0)
            transform.GetComponent<AnimationBehavior>().SendMessage("setMovementState", MovementState.Idle);
    }

}

// This class controls the creation of particular MainPlayer specific objects
internal class PlayerSpecificObject
{
    static ArrayList game_objects = new ArrayList();
    static bool allowed_creation = true;
    static float delay_time = 0.0f;
    Vector3 object_displacement = Vector3.zero;
    GameObject game_object;

    public PlayerSpecificObject(GameObject new_object, string type, Vector3 position, Vector3 init_m_pos, Vector3 final_m_pos)
    {
        game_object = new_object;

        if (type.Equals("Arrow"))
            FireArrow(position, init_m_pos, final_m_pos);
        else if (type.Equals("Melee"))
            DisplayMeleeSwipe(position, init_m_pos, final_m_pos);
    }

    // This converts the position pressed on the Screen to a World Point, determing
    // the proper position and angle to place the MeleeSwipe.
    private void DisplayMeleeSwipe(Vector3 position, Vector3 init_m_pos, Vector3 final_m_pos)
    {
        float angle = 0.0f;
        Vector3 s2wp_finalpos = Vector3.zero;
        Vector3 s2wp_initpos = Vector3.zero;

        if (Camera.main.orthographic)
        {
            s2wp_finalpos = Camera.main.ScreenToWorldPoint(final_m_pos);
            s2wp_initpos = Camera.main.ScreenToWorldPoint(init_m_pos);
        }
        else
        {
            s2wp_finalpos = new Vector3(final_m_pos.x, .5f, final_m_pos.y);
            s2wp_initpos = new Vector3(init_m_pos.x, .5f, init_m_pos.y);
        }

        s2wp_finalpos.y = game_object.transform.position.y;
        s2wp_initpos.y = game_object.transform.position.y;

        Vector3 new_pos = new Vector3((s2wp_finalpos.x - (s2wp_finalpos.x - s2wp_initpos.x) / 2), s2wp_finalpos.y, (s2wp_finalpos.z - (s2wp_finalpos.z - s2wp_initpos.z) / 2));

        if (Camera.main.orthographic)
            object_displacement = 3 * (new Vector3(new_pos.x - position.x, 0, position.z - new_pos.z)).normalized;
        else
            object_displacement = 5 * (new Vector3(new_pos.x - position.x, 0, new_pos.z - position.z)).normalized;

        object_displacement = position + object_displacement;
        object_displacement.y = Camera.main.orthographic ? 0.5f : 3.0f;

        if (object_displacement.z <= position.z)
            angle = Mathf.Rad2Deg * Mathf.Atan((position.x - object_displacement.x) / (position.z - object_displacement.z));
        else
            angle = Mathf.Rad2Deg * (Mathf.PI + Mathf.Atan((position.x - object_displacement.x) / (position.z - object_displacement.z)));

        GameObject new_game_object = (GameObject)GameObject.Instantiate(game_object, object_displacement, Quaternion.Euler(0, angle, 0));
        new_game_object.GetComponent<SwipeBehavior>().setTexture((Texture2D)Resources.Load("Textures/MeleeSwipe_0"));
        game_objects.Add(new_game_object);

        allowed_creation = false;
    }

    // This converts the position pressed on the Screen to a World Point, determing
    // the proper position and angle to place the Arrow.
    private void FireArrow(Vector3 position, Vector3 init_m_pos, Vector3 final_m_pos)
    {
        float angle = 0.0f;
        Vector3 s2wp = Vector3.zero;

        if (Camera.main.orthographic)
        {
            s2wp = Camera.main.ScreenToWorldPoint(final_m_pos);

            object_displacement.x = s2wp.x - position.x;
            object_displacement.y = 0;
            object_displacement.z = position.z - s2wp.z;
        }
        else
        {
            s2wp = new Vector3(final_m_pos.x, .5f, final_m_pos.y);

            object_displacement.x = s2wp.x - position.x;
            object_displacement.y = 0;
            object_displacement.z = s2wp.z - position.z;
        }

        if (s2wp.z <= position.z)
            angle = Mathf.Rad2Deg * Mathf.Atan(object_displacement.x / object_displacement.z);
        else
            angle = Mathf.Rad2Deg * (Mathf.PI + Mathf.Atan(object_displacement.x / object_displacement.z));

        GameObject new_game_object = (GameObject)GameObject.Instantiate(game_object, position, Quaternion.Euler(0, Camera.main.orthographic ? angle : angle + 180, 0));
        new_game_object.GetComponent<ArrowBehavior>().SetProjectileForceDirection((Camera.main.orthographic ? 1 : 5) * (init_m_pos - final_m_pos), object_displacement.normalized);
        game_objects.Add(new_game_object);

        allowed_creation = false;
    }

    // Updates all the active objects that MainPlayer has controlled, deleting them after a particular time interval
    public static void UpdateGameSpecificObjects()
    {
        for (int i = 0; i < game_objects.Count; i++)
        {
            GameObject selected_object = (GameObject)game_objects[i];

            if (selected_object.name.Contains("Arrow") && selected_object.GetComponent<ArrowBehavior>().GetAlphaMod() < 0)
            {
                GameObject.Destroy(selected_object);
                game_objects.Remove(selected_object);
            }
            else if (selected_object.name.Contains("MeleeSwipe") && selected_object.GetComponent<SwipeBehavior>().GetAlphaMod() < 0)
            {
                GameObject.DestroyObject(selected_object);
                game_objects.Remove(selected_object);
            }
        }

        // 0.5 second delay between a MeleeSwipe or Arrow
        if (delay_time > 0.25f && !allowed_creation)
        {
            delay_time = 0;
            allowed_creation = true;
        }
        else if (!allowed_creation)
            delay_time += Time.deltaTime;
    }

    public static bool isAllowedCreation()
    {
        return allowed_creation;
    }
}

/// <summary>
/// Handle touches and swipes on screen. Call delegates when user click on UI
/// </summary>
internal class TouchSwipeController
{
    internal GameObject gameObject;
    internal Transform transform;

    public float PlayerSpeed = 20.0f;
    public OrientationType Orientation;
    public bool ReverseOrientation;
    internal TouchRecorder touchRecorder = new TouchRecorder();
    protected Vector2 active_mousepos = Vector2.zero;
    protected RaycastHit directedRayInfo = new RaycastHit();
    internal Texture2D[] movementControls = new Texture2D[2];
    internal Texture2D[] melee_skill_toggle = new Texture2D[2];
    internal Texture2D[] ranged_skill_toggle = new Texture2D[2];
    internal Texture2D movementControllerPoint;

    internal Rect[] skill_toggle_pos = new Rect[2];
    internal int[,] active_skill_toggle = new int[2,1];
    protected ActiveSkillMode skill_mode = ActiveSkillMode.Melee;
    protected Vector2 center_point = Vector2.zero;
    internal Vector2 init_pos = Vector2.zero;  // initial position of a swipe
    internal Vector2 final_pos = Vector2.zero; // final position of a swipe
    protected float active_angle;
    protected Rect arrow_rect;

    public TouchSwipeController()
    {
        Initialize();
    }

    internal void HandleTouches()
    {
        // Used for Multi-Touch Swiping Determination
        for (int t = 0; t < Input.touchCount; t++)
            HandleOneTouch(t);
    }

    private void Initialize()
    {
        movementControls[0] = (Texture2D)Resources.Load("Textures/MovementControlsRotated");
        movementControls[1] = (Texture2D)Resources.Load("Textures/MovementControls");
        movementControllerPoint = (Texture2D)Resources.Load("Textures/movementPoint");

        if (Orientation.Equals(OrientationType.Portrait))
        {
            melee_skill_toggle[0] = (Texture2D)Resources.Load("Textures/MeleeUnSelected_Rotated");
            melee_skill_toggle[1] = (Texture2D)Resources.Load("Textures/MeleeSelected_Rotated");

            ranged_skill_toggle[0] = (Texture2D)Resources.Load("Textures/RangedUnSelected_Rotated");
            ranged_skill_toggle[1] = (Texture2D)Resources.Load("Textures/RangedSelected_Rotated");
        }
        else if (Orientation.Equals(OrientationType.Landscape))
        {
            melee_skill_toggle[0] = (Texture2D)Resources.Load("Textures/MeleeUnSelected");
            melee_skill_toggle[1] = (Texture2D)Resources.Load("Textures/MeleeSelected");

            ranged_skill_toggle[0] = (Texture2D)Resources.Load("Textures/RangedUnSelected");
            ranged_skill_toggle[1] = (Texture2D)Resources.Load("Textures/RangedSelected");
        }

        // Set initial position for Movement Controls, Melee and Ranged toggles based on Orientation Settings
        if (Orientation.Equals(OrientationType.Portrait) && !ReverseOrientation)
        {
            arrow_rect = new Rect(0, 0, movementControls[0].width, movementControls[0].height);
            
            skill_toggle_pos[0] = new Rect(0, (melee_skill_toggle[0].width / 2) * 4, melee_skill_toggle[0].width / 2, melee_skill_toggle[0].height / 2);
            skill_toggle_pos[1] = new Rect(0, (ranged_skill_toggle[0].width / 2) * 5, ranged_skill_toggle[0].width / 2, ranged_skill_toggle[0].height / 2);
        }
        else if (Orientation.Equals(OrientationType.Portrait) && ReverseOrientation)
        {
            arrow_rect = new Rect(0, Screen.height - movementControls[0].height, movementControls[0].width, movementControls[0].height);

            skill_toggle_pos[0] = new Rect(0, Screen.height - (melee_skill_toggle[0].width / 2) * 5, melee_skill_toggle[0].width / 2, melee_skill_toggle[0].height / 2);
            skill_toggle_pos[1] = new Rect(0, Screen.height - (ranged_skill_toggle[0].width / 2) * 6, ranged_skill_toggle[0].width / 2, ranged_skill_toggle[0].height / 2);
        }
        else if (Orientation.Equals(OrientationType.Landscape) && !ReverseOrientation)
        {
            arrow_rect = new Rect(0, Screen.height - movementControls[0].height, movementControls[0].width, movementControls[0].height);

            skill_toggle_pos[0] = new Rect((melee_skill_toggle[0].width / 2) * 4, Screen.height - 64, melee_skill_toggle[0].width / 2, melee_skill_toggle[0].height / 2);
            skill_toggle_pos[1] = new Rect((ranged_skill_toggle[0].width / 2) * 5, Screen.height - 64, ranged_skill_toggle[0].width / 2, ranged_skill_toggle[0].height / 2);
        }
        else if (Orientation.Equals(OrientationType.Landscape) && ReverseOrientation)
        {
            arrow_rect = new Rect(Screen.width - movementControls[0].width, Screen.height - movementControls[0].height, movementControls[0].width, movementControls[0].height);

            skill_toggle_pos[0] = new Rect(Screen.width - (melee_skill_toggle[0].width / 2) * 5, Screen.height - 64, melee_skill_toggle[0].width / 2, melee_skill_toggle[0].height / 2);
            skill_toggle_pos[1] = new Rect(Screen.width - (ranged_skill_toggle[0].width / 2) * 6, Screen.height - 64, ranged_skill_toggle[0].width / 2, ranged_skill_toggle[0].height / 2);
        }

        center_point = new Vector2(arrow_rect.x + arrow_rect.width / 2, arrow_rect.y + arrow_rect.height / 2);
        DebugPanel.Log("CenterPoint", "Start", center_point);
        DebugPanel.Log("ArrowRect", "Start", arrow_rect);
    }

    protected bool IsTouchedRangedButton(ref Vector2 active_position)
    {
        return skill_toggle_pos[1].Contains(active_position);
    }

    protected bool IsTouchedMeleeButton(ref Vector2 active_position)
    {
        return skill_toggle_pos[0].Contains(active_position);
    }

    protected bool IsUserTouchOutsideMovementButton(ref Vector2 active_position)
    {
        return DistanceToCenter(ref active_position) > HalfMovementButtonWidth();
    }

    protected bool IsUserTouchesMovementButton(ref Vector2 active_position)
    {
        return DistanceToCenter(ref active_position) < HalfMovementButtonWidth();
    }

    private int HalfMovementButtonWidth()
    {
        return movementControls[0].width / 2;
    }

    private float DistanceToCenter(ref Vector2 active_position)
    {
        return Vector2.Distance(center_point, active_position);
    }

    protected Vector2 SpawnArrow(Vector2 active_position)
    {
        if (PlayerSpecificObject.isAllowedCreation() && !SkillTogglesContains(active_position))
            new PlayerSpecificObject((GameObject)Resources.Load("Prefabs/ArrowTemplate"), "Arrow", transform.position, init_pos, final_pos);
        return active_position;
    }

    private bool SkillTogglesContains(Vector2 mouse_pos)
    {
        for (int i = 0; i < skill_toggle_pos.Length; i++)
        {
            if (skill_toggle_pos[i].Contains(mouse_pos))
                return true;
        }

        return false;
    }

    private Vector2 FindFinalPosition(Vector2 active_position)
    {
        if (Camera.main.orthographic)
            final_pos = active_position;
        else
            final_pos = active_mousepos;
        return active_position;
    }

    private static bool IsTouchBegan(int touch)
    {
        return Input.GetTouch(touch).phase == TouchPhase.Began;
    }

    protected bool IsRangedSelected()
    {
        return skill_mode == ActiveSkillMode.Ranged;
    }

    private Vector2 HandleMeleeSwingTouch(int touch, Vector2 active_position)
    {
        if (IsTouchBegan(touch))
        {
            active_position = SetInitPosition(active_position);
        }
        else if (IsTouchEnded(touch))
        {
            active_position = SetFinalPosition(active_position);

            active_position = SpawnMeleeSwipe(active_position);
        }
        return active_position;
    }

    protected Vector2 SpawnMeleeSwipe(Vector2 active_position)
    {
        DebugPanel.Log("MeleeSwipePos", "Swipe", transform.position);
        // Only Do a MeleeSwipe if the magnitude is greater than a value
        if (((final_pos - init_pos).magnitude >= 30) && (PlayerSpecificObject.isAllowedCreation() && !SkillTogglesContains(active_position)))
            new PlayerSpecificObject((GameObject)Resources.Load("Prefabs/MeleeSwipe"), "Melee", transform.position, init_pos, final_pos);
        return active_position;
    }

    protected Vector2 SetFinalPosition(Vector2 active_position)
    {
        if (Camera.main.orthographic)
            final_pos = active_position;
        else
            final_pos = active_mousepos;
        DebugPanel.Log("FinalPos", "Handled", final_pos);
        return active_position;
    }

    protected Vector2 SetInitPosition(Vector2 active_position)
    {
        if (Camera.main.orthographic)
            init_pos = active_position;
        else
            init_pos = active_mousepos;
        DebugPanel.Log("InitPos", "Handled", init_pos);
        return active_position;
    }

    protected bool IsMeleeSelected()
    {
        return skill_mode == ActiveSkillMode.Melee;
    }

    public delegate void TouchEventHandler( Vector2 activeMousePos );

    public event TouchEventHandler HandleTouch;

    private Vector2 HandleTouchMovement(int touch, Vector2 active_position)
    {
        if (IsUserTouchesMovementButton(ref active_position))
        {
            active_mousepos = GetTouchPositionOnScreen(touch);
            HandleTouch(active_mousepos);
        }
        return active_position;
    }

    internal void HandleOneTouch(int touch)
    {
        Vector2 active_position = GetTouchPositionOnScreen(touch);

        HandleTouchMovement(touch, active_position);

        HandleTouchTrails(touch, active_position);

        HandleTouchCombatModeSwitch(touch, active_position);
    }

    private Vector2 HandleTouchTrails(int touch, Vector2 active_position)
    {
        if (IsUserTouchOutsideMovementButton(ref active_position))
        {
            HandleTouchWithTrail(touch, active_position);
        }
        return active_position;
    }


    protected void SwitchToRanged()
    {
        SetSelectedToggle(ActiveSkillMode.Ranged);
    }

    protected void SwitchToMelee()
    {
        SetSelectedToggle(ActiveSkillMode.Melee);
    }

    private Vector2 HandleTouchCombatModeSwitch(int touch, Vector2 active_position)
    {
        if (IsTouchSwitchToMelee(touch, ref active_position))
        {
            SwitchToMelee();
        }
        else if (IsTouchSwitchToRanged(touch, ref active_position))
        {
            SwitchToRanged();
        }
        return active_position;
    }

    private static Vector2 GetTouchPositionOnScreen(int touch)
    {
        Vector2 active_position = new Vector2(Input.GetTouch(touch).position.x, Screen.height - Input.GetTouch(touch).position.y);
        return active_position;
    }

    private void SetMousePositionFromTouch(int touch)
    {
        if (Camera.main.orthographic)
            SetMousePositionFromTouchOrtho(touch);
        else
        {
            SetMousePositionFromTouchPerspective(touch);
        }
    }

    private Vector2 HandleWeaponUsageTouch(int touch, Vector2 active_position)
    {
        if (IsMeleeSelected())
        {
            active_position = HandleMeleeSwingTouch(touch, active_position);
        }
        else if (IsRangedSelected())
        {
            active_position = HandleRangedStrikeTouch(touch, active_position);
        }
        return active_position;
    }

    private Vector2 HandleRangedStrikeTouch(int touch, Vector2 active_position)
    {
        if (IsTouchBegan(touch))
        {
            if (Camera.main.orthographic)
                init_pos = active_position;
            else
                init_pos = active_mousepos;
        }
        else if (IsTouchEnded(touch))
        {
            active_position = FindFinalPosition(active_position);

            active_position = SpawnArrow(active_position);
        }
        return active_position;
    }

    private bool IsTouchSwitchToRanged(int touch, ref Vector2 active_position)
    {
        return IsTouchEnded(touch) && IsTouchedRangedButton(ref active_position);
    }

    private bool IsTouchSwitchToMelee(int touch, ref Vector2 active_position)
    {
        return IsTouchEnded(touch) && IsTouchedMeleeButton(ref active_position);
    }
    private void SetMousePositionFromTouchPerspective(int touch)
    {
        Ray directedRay = Camera.main.ScreenPointToRay(new Vector2(Input.GetTouch(touch).position.x, Input.GetTouch(touch).position.y));
        if (Physics.Raycast(directedRay, out directedRayInfo))
        {
            //Debug.DrawRay(directedRay.origin, directedRay.direction, Color.red, 1);
            active_mousepos = new Vector2(directedRayInfo.point.x, directedRayInfo.point.z);
        }
    }

    private void SetMousePositionFromTouchOrtho(int touch)
    {
        active_mousepos = new Vector2(Input.GetTouch(touch).position.x, Input.GetTouch(touch).position.y);
    }
    private Vector2 HandleTouchWithTrail(int touch, Vector2 active_position)
    {
        SetMousePositionFromTouch(touch);

        HandleWeaponUsageTouch(touch, active_position);

        if (IsTouchMoved(touch))
        {
            HandleTouchMoved();
        }
        else if (IsTouchEnded(touch))
        {
            HandleTouchEnded();
        }
        return active_position;
    }

    private static bool IsTouchEnded(int touch)
    {
        return Input.GetTouch(touch).phase == TouchPhase.Ended;
    }

    private void DirectionDetermination()
    {
        // Change these particular basis behaviors to your desired outcome within a particular case statement
        // NOTE: Within a Portrait Perspective (Android Build), the Directions are rotated 90 degrees. 
        // Meaning, the ShapeType.Right will now behave like the ShapeType.Up, ShapeType.Down will now behave
        // like ShapeType.Left.
        switch ((ShapeType)touchRecorder.getDeterminedShape)
        {
            case ShapeType.SinglePoint:
                Debug.Log("Single Point.");
                break;

            case ShapeType.DownLeft:
                Debug.Log("Down Left");
                break;

            case ShapeType.DownRight:
                Debug.Log("Down Right");
                break;

            case ShapeType.UpRight:
                Debug.Log("Up Right");
                break;

            case ShapeType.UpLeft:
                Debug.Log("Up Left");
                break;

            case ShapeType.Left:
                Debug.Log("Left");
                break;

            case ShapeType.Down:
                Debug.Log("Down");
                break;

            case ShapeType.Right:
                Debug.Log("Right");
                break;

            case ShapeType.Up:
                Debug.Log("Up");
                break;

            default:
                Debug.Log("Undefined.");
                break;
        }
    }

    private Vector3 CalculateMouseInWorld(Vector3 calculatedPoint)
    {
        if (Camera.main.orthographic)
            calculatedPoint = CalculateMouseInWordOrtho(calculatedPoint);
        else
            calculatedPoint = CalculateMouseInWorldPerspective(calculatedPoint);
        return calculatedPoint;
    }

    private Vector3 CalculateMouseInWorldPerspective(Vector3 calculatedPoint)
    {
        calculatedPoint = new Vector3(active_mousepos.x, .5f, active_mousepos.y);
        return calculatedPoint;
    }

    private Vector3 CalculateMouseInWordOrtho(Vector3 calculatedPoint)
    {
        calculatedPoint = Camera.main.ScreenToWorldPoint(active_mousepos);
        return calculatedPoint;
    }

    private void AddTouchWithTrail()
    {
        touchRecorder.addTouch(active_mousepos);

        Vector3 calculatedPoint = Vector3.zero;
        CalculateMouseInWorld(calculatedPoint);

        touchRecorder.getTrailRenderer().transform.position = new Vector3(calculatedPoint.x, .5f, calculatedPoint.z);
    }

    private void HandleOldTouchMoveWithTrail()
    {
        touchRecorder.setNewTouch(true);
        touchRecorder.setTrailRendererObject(Resources.Load("Prefabs/SwipeTrail"));

        touchRecorder.addTouch(active_mousepos);

        Vector3 calculatedPoint = Vector3.zero;
        CalculateMouseInWorld(calculatedPoint);

        touchRecorder.getTrailRenderer().transform.position = new Vector3(calculatedPoint.x, .5f, calculatedPoint.z);
    }

    private static bool IsTouchMoved(int touch)
    {
        return Input.GetTouch(touch).phase == TouchPhase.Moved;
    }

    
    protected void HandleTouchMoved()
    {
        if (IsItAnOldTouch())
        {
            HandleOldTouchMoveWithTrail();
        }

        if (touchRecorder.isAllowedAdd())
        {
            AddTouchWithTrail();
        }
    }

    private bool IsItAnOldTouch()
    {
        return !touchRecorder.isNewTouch();
    }

    protected void HandleTouchEnded()
    {
        touchRecorder.addTouch(active_mousepos);

        Vector3 calculatedPoint = Vector3.zero;
        calculatedPoint = CalculateMouseInWorld(calculatedPoint);

        touchRecorder.getTrailRenderer().transform.position = new Vector3(calculatedPoint.x, .5f, calculatedPoint.z);
        touchRecorder.determineShapeApproximation();

        // Determines the Direction of a particular swipe
        DirectionDetermination();

        touchRecorder.Reset();
    }

    internal void SetSelectedToggle(ActiveSkillMode s_mode)
    {
        active_skill_toggle[(int)skill_mode, 0] = 0;
        skill_mode = s_mode;
        active_skill_toggle[(int)skill_mode, 0] = 1;
    }


    internal float FindActiveAngle()
    {
        float angle = 0.0f;
        float dx = 0.0f;
        float dy = 0.0f;

        if (center_point.x <= active_mousepos.x)
        {
            dy = center_point.y - active_mousepos.y;
            dx = active_mousepos.x - center_point.x;

            if (dx == 0)
                dx = 0.01f;

            angle = Mathf.Atan(dy / dx);
            active_angle = angle;
        }
        else
        {
            dy = active_mousepos.y - center_point.y;
            dx = center_point.x - active_mousepos.x;

            if (dx == 0)
                dx = 0.01f;

            angle = Mathf.PI + Mathf.Atan(dy / dx);
            active_angle = angle;
        }

        return active_angle;
    }
}


/// <summary>
/// Simulates swipes with mouse only pc
/// </summary>
internal class MouseTouchSimulator : TouchSwipeController
{
 
    public delegate void MouseHandler( Vector2 activeMousePos );
    public event MouseHandler HandleMouse; 

    internal void Handle()
    {
        Vector2 active_position = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        DebugPanel.Log("ActivePositionS", "Start", active_position);

        if (IsLeftMouseButtonPressed() && IsUserTouchesMovementButton(ref active_position))
        {
            SetMousePositionFromScreenBottom();
            HandleMouse(active_mousepos);
        }

        // Only do this if the swipe isn't within the Movement Controller
        if (IsUserTouchOutsideMovementButton(ref active_position))
        {
            active_position = HandleMouseWithTrail(active_position);
        }

        if (IsLeftMouseButtonPressed() && IsTouchedMeleeButton( ref active_position ))
        {
            SwitchToMelee();
        }
        else if (IsLeftMouseButtonReleased() && IsTouchedRangedButton(ref active_position) )
        {
            SwitchToRanged();
        }
    }

    
    private void SetMousePositionOrtho()
    {
        active_mousepos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
    }

    private void SetMousePositionFromScreenBottom()
    {
        active_mousepos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
    }

    private static bool IsLeftMouseButtonPressed()
    {
        return Input.GetMouseButton(0);
    }


    private Vector2 HandleMouseWithTrail(Vector2 active_position)
    {
        SetMousePosition();

        active_position = HandleWeaponUsageMouse(active_position);

        if (IsLeftMouseButtonPressed())
        {
            HandleTouchMoved();
        }
        else if (IsLeftMouseButtonReleased())
        {
            HandleTouchEnded();
        }
        return active_position;
    }

    private Vector2 HandleWeaponUsageMouse(Vector2 active_position)
    {
        if (IsMeleeSelected())
        {
            active_position = HandleMeleeSwingMouse(active_position);
        }
        else if (IsRangedSelected())
        {
            active_position = HandleRangedStrikeMouse(active_position);
        }

        return active_position;
    }

    private Vector2 HandleRangedStrikeMouse(Vector2 active_position)
    {
        if (IsLeftMouseButtonPressed())
        {
            SetInitPosition(active_position);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            SetFinalPosition(active_position);
            SpawnArrow(active_position);
        }
        return active_position;
    }

    private Vector2 HandleMeleeSwingMouse(Vector2 active_position)
    {
        if (IsLeftMouseButtonPressed())
        {
            active_position = SetInitPosition(active_position);
        }
        else if (IsLeftMouseButtonReleased())
        {
            active_position = SetFinalPosition(active_position);

            SpawnMeleeSwipe(active_position);
        }
        return active_position;
    }

    private static bool IsLeftMouseButtonReleased()
    {
        return Input.GetMouseButtonUp(0);
    }

    private void SetMousePosition()
    {
        if (Camera.main.orthographic)
            SetMousePositionOrtho();
        else
        {
            SetMousePositionPerspective();
        }
    }

    private void SetMousePositionPerspective()
    {
        Ray directedRay = Camera.main.ScreenPointToRay(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
        if (Physics.Raycast(directedRay, out directedRayInfo))
        {
            active_mousepos = new Vector2(directedRayInfo.point.x, directedRayInfo.point.z);
        }
    }


    internal void DrawGUI()
    {
        if (Orientation.Equals(OrientationType.Portrait) && !ReverseOrientation)
            GUI.DrawTexture(arrow_rect, movementControls[0]);
        else if (Orientation.Equals(OrientationType.Portrait) && ReverseOrientation)
            GUI.DrawTexture(arrow_rect, movementControls[0]);
        else if (Orientation.Equals(OrientationType.Landscape) && !ReverseOrientation)
            GUI.DrawTexture(arrow_rect, movementControls[1]);
        else if (Orientation.Equals(OrientationType.Landscape) && ReverseOrientation)
            GUI.DrawTexture(arrow_rect, movementControls[1]);

        GUI.DrawTexture(skill_toggle_pos[(int)ActiveSkillMode.Melee], melee_skill_toggle[active_skill_toggle[(int)ActiveSkillMode.Melee, 0]]);
        GUI.DrawTexture(skill_toggle_pos[(int)ActiveSkillMode.Ranged], ranged_skill_toggle[active_skill_toggle[(int)ActiveSkillMode.Ranged, 0]]);

        GUI.DrawTexture(new Rect(active_mousepos.x - movementControllerPoint.width / 2, active_mousepos.y - movementControllerPoint.height / 2, movementControllerPoint.width, movementControllerPoint.height), movementControllerPoint);

        //Only for mouse
        if ((Orientation.Equals(OrientationType.Portrait) && ReverseOrientation) || Orientation.Equals(OrientationType.Landscape))
            GUI.Label(new Rect(0, 0, 320, 40), "Desktop Test Mode\n(Disable In MainPlayer -> PlayerController Inspector)");
        else
            GUI.Label(new Rect(0, Screen.height - 40, 320, 40), "Desktop Test Mode\n(Disable In MainPlayer -> PlayerController Inspector)");
    }

    internal void SetSkillToggle()
    {
        SetSelectedToggle(skill_mode);        
    }

}

public class PlayerController : MonoBehaviour
{

    public float PlayerSpeed = 20.0f;
    public OrientationType Orientation;
    public bool ReverseOrientation;
    public bool DesktopTestMode = true;

    private MouseTouchSimulator mouseToucher;
    private TouchSwipeController touchController;
    private PlayerMover mover;

    TouchRecorder touchRecorder = new TouchRecorder();

    TouchControllerBase toucher;

    void Start()
    {
        mouseToucher = new MouseTouchSimulator();
        mouseToucher.Orientation = Orientation;
        mouseToucher.ReverseOrientation = ReverseOrientation;
        mouseToucher.PlayerSpeed = PlayerSpeed;
        mouseToucher.gameObject = gameObject;
        mouseToucher.transform = transform;

        mouseToucher.SetSkillToggle();

        mouseToucher.HandleTouch += mouseToucher_HandleTouch;
        mouseToucher.HandleMouse += mouseToucher_HandleMouse;

        toucher = new TouchControllerBase();

        mover = new PlayerMover();
        mover.transform = transform;
        mover.toucher = mouseToucher;
        mover.gameObject = gameObject;
    }

    void mouseToucher_HandleMouse(Vector2 activeMousePos)
    {
        mover.active_movement_toggle = true;
    }

    void mouseToucher_HandleTouch(Vector2 activeMousePos)
    {
        mover.active_movement_toggle = true;
    }

    void OnGUI()
    {
        mouseToucher.DrawGUI();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        mover.MoveIn2D();

        mover.MoveOrStopPhysically();

        if (DesktopTestMode)
            HandleDesktopMouse();
        else
            HandleTouchScreen();

        PlayerSpecificObject.UpdateGameSpecificObjects();

        if (touchRecorder.isNewTouch())
            touchRecorder.UpdateRecorder();
    }

    private void HandleTouchScreen()
    {
        mouseToucher.HandleTouches();
    }

    private void HandleDesktopMouse()
    {
        mouseToucher.Handle();
    }




}
