#include <Windows.h>
#include <Shlobj.h>
#include <tchar.h>
#include <stdio.h>


extern "C"
{

HWND ghWndKeyboard;
HHOOK g_HookKeyPosted = NULL;
HHOOK g_HookKeyCalled = NULL;
extern HINSTANCE g_hInst;
BOOL gKeyBoardUp = FALSE;
FILE *g_ofp;
DWORD gProcessID;

BOOL FileExists(LPCTSTR szPath)
{
	DWORD dwAttrib = GetFileAttributes(szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES && !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

#define SHOW_KEYBOARD_SHOWN				0x0001
#define SHOW_KEYBOARD_NOT_SHOWN			0x0002
#define SHOW_KEYBOARD_SUCCESS_TABTIP	0x0004
#define SHOW_KEYBOARD_SUCCESS_OSK		0x0008
#define SHOW_KEYBOARD_ERROR_TABTIP		0x0010
#define SHOW_KEYBOARD_ERROR_OSK			0x0020
#define SHOW_KEYBOARD_NOT_FOUND_TABTIP	0x0040
#define SHOW_KEYBOARD_NOT_FOUND_OSK		0x0080

// General show keyboard method - CYIM
__declspec(dllexport) int __cdecl W8_ShowKeyboard()
{
	int status = 0;

	STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

	// Default full path to the touch keyboard for Windows 8
	TCHAR tabTipPath[MAX_PATH];
	SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, tabTipPath);
	// CYIM - Default to 64 bit Program Files
	TCHAR *index = _tcsstr(tabTipPath, " (x86)");
	if (index != NULL)
	{
		_tcscpy_s(index, _tcslen(index), "\0");	// Remove (x86) from Program Files path
	}
	_tcscat_s(tabTipPath, "\\Common Files\\Microsoft Shared\\ink\\TabTip.exe");

	if (FileExists(tabTipPath))
	{
		HINSTANCE result = ShellExecute( NULL,
											NULL,
											tabTipPath,
											NULL,
											NULL,
											SW_SHOW );
		// result is not a true HINSTANCE, just an error code.  result > 32 is success.
		if ((int)result > 32)
		{

			// Success with TabTip
			status |= SHOW_KEYBOARD_SHOWN;
			status |= SHOW_KEYBOARD_SUCCESS_TABTIP;
			return status;
		}

		// Error with running TabTip
		status |= SHOW_KEYBOARD_ERROR_TABTIP;
	}
	else
	{
		status |= SHOW_KEYBOARD_NOT_FOUND_TABTIP;
	}
	
	// Couldn't find or there was an error running the Windows 8 keyboard, default to Windows 7 / 8 keyboard...
	TCHAR oskPath[MAX_PATH];
	SHGetFolderPath(NULL, CSIDL_SYSTEM, NULL, 0, oskPath);
	_tcscat_s(oskPath, "\\osk.exe");

	if (FileExists(oskPath))
	{
		HINSTANCE result = ShellExecute( NULL,
											NULL,
											oskPath,
											NULL,
											NULL,
											SW_SHOW );
		// result is not a true HINSTANCE, just an error code.  result > 32 is success.
		if ((int)result > 32)
		{
			// Success with OSK
			status |= SHOW_KEYBOARD_SHOWN;
			status |= SHOW_KEYBOARD_SUCCESS_OSK;
			return status;
		}

		// Error with running OSK
		status |= SHOW_KEYBOARD_ERROR_OSK;
	}
	else
	{
		status |= SHOW_KEYBOARD_NOT_FOUND_OSK;
	}

	// Unable to find any virtual keyboard program
	status |= SHOW_KEYBOARD_NOT_SHOWN;
	return status;
}


// Hook prok to capture calls to Unity Window function by System
LRESULT KeybHookProcCalled(int code, WPARAM wParam, LPARAM lParam)
{
	CWPSTRUCT *Msg = (CWPSTRUCT *)lParam;
	fprintf(g_ofp,"HookCalled: %d\n", Msg->message);
	switch (Msg->message)
	{
	case WM_ENABLE:
		gKeyBoardUp = (BOOL)Msg->wParam;
		break;
	}
	// Always call this - we dont actually "Use" any of the messages
	return CallNextHookEx(0, code, wParam, lParam); 
}


// Hook prok to capture messages Posted to Unity Window
LRESULT KeybHookProcPosted(int code, WPARAM wParam, LPARAM lParam)
{
	MSG *Msg = (MSG *)lParam;
	fprintf(g_ofp,"HookPosted: %d\n", Msg->message);
	switch (Msg->message)
	{
	case WM_ENABLE:
		gKeyBoardUp = (BOOL)Msg->wParam;
		break;
	}
	// Always call this - we dont actually "Use" any of the messages
	return CallNextHookEx(0, code, wParam, lParam); 
}

__declspec(dllexport) void __cdecl CloseFile()
{
	fclose(g_ofp);
}

__declspec(dllexport) BOOL __cdecl W8_IsKeyBoardUp()
{
	return gKeyBoardUp;
}

__declspec(dllexport) DWORD __cdecl GetPID()
{
	return gProcessID;
}

__declspec(dllexport) int __cdecl W8_HookKeyboard()
{

	g_ofp = fopen("MessageLog.txt", "w");

	if (g_ofp != NULL) {

	}

	ghWndKeyboard = FindWindow("IPTip_Main_Window", NULL);
	if (ghWndKeyboard == NULL)
	{
		return -1;
	}

	// Get thread ID for Unity window
	int ID = GetWindowThreadProcessId(ghWndKeyboard, &gProcessID);  
	if (ID == 0)
		return -2;

	// Set the Hooks 
	// One for Posted Messages
	g_HookKeyPosted = SetWindowsHookEx(WH_GETMESSAGE, (HOOKPROC)KeybHookProcPosted, g_hInst, ID); 
	if (g_HookKeyPosted == NULL)
	{
		return -3;
	}

	// One hook for System calls (Win8)
	g_HookKeyCalled = SetWindowsHookEx(WH_CALLWNDPROC, (HOOKPROC)KeybHookProcCalled, g_hInst, ID); 
	if (g_HookKeyCalled == NULL)
	{
		UnhookWindowsHookEx(g_HookKeyPosted);
		return -4;
	}

	return 0;

}
// Note: This only handles the "tabtip.exe" keyboard
__declspec(dllexport) int __cdecl W8_HideKeyboard()
{
	HWND hWndKeyboard = FindWindow("IPTip_Main_Window", NULL);
	if (hWndKeyboard == NULL)
	{
		return -1;
	}

	if (PostMessage(hWndKeyboard, WM_SYSCOMMAND, SC_CLOSE, 0))
	{
		// Success!
		return 0;
	}

	return GetLastError();
}

// Note: This only handles the "tabtip.exe" keyboard
__declspec(dllexport) int __cdecl W8_IsKeyboardVisible()
{
	HWND hWndKeyboard = FindWindow("IPTip_Main_Window", NULL);
	if (hWndKeyboard == NULL)
	{
		return -1;
	}

	if(IsWindowVisible(hWndKeyboard))
		return 1;
	else 
		return 0;

	return GetLastError();
}


// Note: This only handles the "tabtip.exe" keyboard
__declspec(dllexport) int __cdecl W8_GetKeyboardStatus2()
{
	HWND hWndKeyboard = FindWindow("IPTip_Main_Window", NULL);
	if (hWndKeyboard == NULL)
	{
		return -1;
	}

	WINDOWINFO winfo;
	winfo.cbSize = sizeof(WINDOWINFO);

	if( GetWindowInfo(hWndKeyboard, &winfo) ){

		return (unsigned int) winfo.dwWindowStatus;
	}

	return GetLastError();
}

// Note: This only handles the "tabtip.exe" keyboard
__declspec(dllexport) int __cdecl W8_GetKeyboardStatus()
{
	HWND hWndKeyboard = FindWindow("IPTip_Main_Window", NULL);
	if (hWndKeyboard == NULL)
	{
		return -1;
	}

	WINDOWPLACEMENT wplace;
	wplace.length = sizeof(WINDOWPLACEMENT);

	if( GetWindowPlacement(hWndKeyboard, &wplace) ){

		return (unsigned int) wplace.showCmd;
	}

	return GetLastError();
}

// "attempt" to bring up the OSK (on screen keyboard) - CYIM
__declspec(dllexport) int __cdecl W8_ShowTabTip()
{
	int status = 0;

	STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

	// Default full path to the touch keyboard for Windows 8
	TCHAR tabTipPath[MAX_PATH];
	SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, 0, tabTipPath);
	// CYIM - Default to 64 bit Program Files
	TCHAR *index = _tcsstr(tabTipPath, " (x86)");
	if (index != NULL)
	{
		_tcscpy_s(index, _tcslen(index), "\0");	// Remove (x86) from Program Files path
	}
	_tcscat_s(tabTipPath, "\\Common Files\\Microsoft Shared\\ink\\TabTip.exe");

	if (FileExists(tabTipPath))
	{
		HINSTANCE result = ShellExecute( NULL,
											NULL,
											tabTipPath,
											NULL,
											NULL,
											SW_SHOW );
		// result is not a true HINSTANCE, just an error code.  result > 32 is success.
		if ((int)result > 32)
		{
			// Success with TabTip
			status |= SHOW_KEYBOARD_SUCCESS_TABTIP;
			return status;
		}

		// Error with running TabTip
		status |= SHOW_KEYBOARD_ERROR_TABTIP;
	}
	else
	{
		status |= SHOW_KEYBOARD_NOT_FOUND_TABTIP;
	}
	
	status |= SHOW_KEYBOARD_NOT_SHOWN;
	return status;
}

// "attempt" to bring up the alt OSK (on screen keyboard) - CYIM
__declspec(dllexport) int __cdecl W8_ShowOSK()
{
	int status = 0;

	STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));
	
	// Couldn't find or there was an error running the Windows 8 keyboard, default to Windows 7 / 8 keyboard...
	TCHAR oskPath[MAX_PATH];
	SHGetFolderPath(NULL, CSIDL_SYSTEM, NULL, 0, oskPath);
	_tcscat_s(oskPath, "\\osk.exe");

	if (FileExists(oskPath))
	{
		HINSTANCE result = ShellExecute( NULL,
											NULL,
											oskPath,
											NULL,
											NULL,
											SW_SHOW );
		// result is not a true HINSTANCE, just an error code.  result > 32 is success.
		if ((int)result > 32)
		{
			// Success with OSK
			status |= SHOW_KEYBOARD_SUCCESS_OSK;
			return status;
		}

		// Error with running OSK
		status |= SHOW_KEYBOARD_ERROR_OSK;
	}
	else
	{
		status |= SHOW_KEYBOARD_NOT_FOUND_OSK;
	}

	status |= SHOW_KEYBOARD_NOT_SHOWN;
	return status;
}

}