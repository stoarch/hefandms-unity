//--------------------------------------------------------------------------------------
// Copyright 2013 Intel Corporation
// All Rights Reserved
//
// Permission is granted to use, copy, distribute and prepare derivative works of this
// software for any purpose and without fee, provided, that the above copyright notice
// and this statement appear in all copies.  Intel makes no representations about the
// suitability of this software for any purpose.  THIS SOFTWARE IS PROVIDED "AS IS."
// INTEL SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, AND ALL LIABILITY,
// INCLUDING CONSEQUENTIAL AND OTHER INDIRECT DAMAGES, FOR THE USE OF THIS SOFTWARE,
// INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PROPRIETARY RIGHTS, AND INCLUDING THE
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  Intel does not
// assume any responsibility for any errors which may appear in this software nor any
// responsibility to update it.
//--------------------------------------------------------------------------------------

#include "WindowsMobilityAPIs.h"


MobilityMonitor::MobilityMonitor()
{
	isValid = false;

	ReadCurrentScheme();
}

MobilityMonitor::~MobilityMonitor()
{

}


int MobilityMonitor::Register(HWND hWnd)
{
	HPOWERNOTIFY retval = NULL;

	retval = RegisterPowerSettingNotification(hWnd, &GUID_POWERSCHEME_PERSONALITY, DEVICE_NOTIFY_WINDOW_HANDLE);

	if( !retval )
		return 0;

	retval = NULL;
	retval = RegisterPowerSettingNotification(hWnd, &GUID_ACDC_POWER_SOURCE, DEVICE_NOTIFY_WINDOW_HANDLE);

	if( !retval )
		return 0;

	retval = NULL;
	retval = RegisterPowerSettingNotification(hWnd, &GUID_MONITOR_POWER_ON, DEVICE_NOTIFY_WINDOW_HANDLE);

	if( !retval )
		return 0;

	retval = NULL;
	retval = RegisterPowerSettingNotification(hWnd, &GUID_BATTERY_PERCENTAGE_REMAINING, DEVICE_NOTIFY_WINDOW_HANDLE);

	if( !retval )
		return 0;

	//success
	return 1;
}

void MobilityMonitor::Poll()
{
	if (!GetSystemPowerStatus(&pwrStat))
	{
		isValid = false;
	}
	isValid = true;
}


bool MobilityMonitor::IsLaptop()
{
	bool bLaptop =  false;
	Poll();
	if(isValid)
	{
		int BatStat = (int)pwrStat.BatteryFlag;

		if (BatStat == 128)
		{
			bLaptop = false;
		}
		else
		{
			bLaptop = true;
		}
	}
	return bLaptop;
}

PowerSource MobilityMonitor::GetPowerSrc()
{
	PowerSource retVal = Undefined_Power;
	Poll();
	if (!isValid)
	{
		return retVal;
	}
	BYTE acLine = pwrStat.ACLineStatus;
	switch((int)acLine)
	{
	case 0:
		retVal = Battery_Power;
		break;
	case 1:
		retVal = AC_Power;
		break;
	case 255:
		retVal = Undefined_Power;
		break;
	default:
		retVal = Undefined_Power;
		break;
	}
	return retVal;
}


/**
*	This functions returns percent battery life indicating how much
	battery life is left. Common usage of this function is that it should
	be called when running on battery only. This can be called when running
	on AC power as well.
	Returns -1 if function fails
*/
int MobilityMonitor::GetPercentBatteryLife()
{
	Poll();
	if (!isValid)
	{
		return -1;
	}
	int BatLife = (int)pwrStat.BatteryLifePercent;

	return BatLife;
}


/**
*	This function returns battery life left in seconds.
	Common usage of this function is that it should
	be called when running on battery only
	Returns -1 when function call fails
*/	
unsigned long MobilityMonitor::GetSecBatteryLifeTimeRemaining()
{
	Poll();

	if (!isValid)
	{
		return -1;
	}
	DWORD BatLifeTime = pwrStat.BatteryLifeTime;

	if ((BatLifeTime/3600) > 24)
	{
		return -1;
	}

	return BatLifeTime;
}




void MobilityMonitor::ReadCurrentScheme()
{
	GUID *scheme;
	DWORD displayBufferSize = sizeof(PowerSchemeName);

	if(ERROR_SUCCESS == PowerGetActiveScheme(NULL, &scheme))
	{
		GUID buffer;
		DWORD bufferSize = sizeof(buffer);

		if(ERROR_SUCCESS == PowerReadFriendlyName(NULL, scheme, NULL, NULL, (UCHAR*)PowerSchemeName, 	&displayBufferSize))
		{
			OutputDebugString((char*)PowerSchemeName);
		}
		LocalFree(scheme);
	}
}