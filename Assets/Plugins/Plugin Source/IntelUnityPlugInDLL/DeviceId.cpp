// Copyright 2010 Intel Corporation
// All Rights Reserved
//
// Permission is granted to use, copy, distribute and prepare derivative works of this
// software for any purpose and without fee, provided, that the above copyright notice
// and this statement appear in all copies.  Intel makes no representations about the
// suitability of this software for any purpose.  THIS SOFTWARE IS PROVIDED ""AS IS.""
// INTEL SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, AND ALL LIABILITY,
// INCLUDING CONSEQUENTIAL AND OTHER INDIRECT DAMAGES, FOR THE USE OF THIS SOFTWARE,
// INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PROPRIETARY RIGHTS, AND INCLUDING THE
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  Intel does not
// assume any responsibility for any errors which may appear in this software nor any
// responsibility to update it.


//
// DeviceId.cpp : Implements the GPU Device detection and graphics settings
//                configuration functions.
//

#include "DeviceId.h"
#include <stdio.h>
#include <tchar.h>

#include <D3D11.h>
#include <DxErr.h>
#include <D3DCommon.h>
#include <DXGI.h>
#include <D3D9.h>

#include <oleauto.h>
#include <initguid.h>
#include <wbemidl.h>
#include <ObjBase.h>

static const int FIRST_GFX_ADAPTER = 0;


/*****************************************************************************************
 * getDXGIAdapterDesc
 *
 *     Function to get the a DXGI_ADAPTER_DESC structure for a given device.
 *
 *****************************************************************************************/

bool getDXGIAdapterDesc(DXGI_ADAPTER_DESC* AdapterDesc, unsigned int adapterNum = FIRST_GFX_ADAPTER)
{
	bool retVal = false;
	bool bHasWDDMDriver = false;
	if(AdapterDesc == NULL)
		return false;
	HMODULE hD3D9 = LoadLibrary( "d3d9.dll" );
	if( hD3D9 == NULL )
		return false;

	/*
		* Try to create IDirect3D9Ex interface (also known as a DX9L interface). 
		* This interface can only be created if the driver is a WDDM driver.
		*/

	// Define a function pointer to the Direct3DCreate9Ex function.
	typedef HRESULT ( WINAPI*LPDIRECT3DCREATE9EX )( UINT,
													void** );

	// Obtain the address of the Direct3DCreate9Ex function.
	LPDIRECT3DCREATE9EX pD3D9Create9Ex = NULL;
	pD3D9Create9Ex = ( LPDIRECT3DCREATE9EX )GetProcAddress( hD3D9, "Direct3DCreate9Ex" );

	bHasWDDMDriver = ( pD3D9Create9Ex != NULL );

	if( bHasWDDMDriver )
	{
		// Has WDDM Driver (Vista, and later)
		HMODULE hDXGI = NULL;

		hDXGI = LoadLibrary( "dxgi.dll" );

		// DXGI libs should really be present when WDDM driver present.
		if( hDXGI )
		{
			// Define a function pointer to the CreateDXGIFactory1 function.
			typedef HRESULT ( WINAPI*LPCREATEDXGIFACTORY )( REFIID riid,
															void** ppFactory );

			// Obtain the address of the CreateDXGIFactory1 function.
			LPCREATEDXGIFACTORY pCreateDXGIFactory = NULL;
			pCreateDXGIFactory = ( LPCREATEDXGIFACTORY )GetProcAddress( hDXGI, "CreateDXGIFactory" );

			if( pCreateDXGIFactory )
			{
				// Got the function hook from the DLL
				// Create an IDXGIFactory object.
				IDXGIFactory* pFactory;
				if( SUCCEEDED( ( *pCreateDXGIFactory )( __uuidof( IDXGIFactory ), ( void** )( &pFactory ) ) ) )
				{
					// Enumerate adapters. Code here only gets the info for the first adapter.
					// If secondary or multiple Gfx adapters will be used, the code needs to be 
					// modified to accomodate that.
					IDXGIAdapter* pAdapter;
					if( SUCCEEDED( pFactory->EnumAdapters( FIRST_GFX_ADAPTER, &pAdapter ) ) )
					{
						pAdapter->GetDesc( AdapterDesc );
						pAdapter->Release();

						retVal = true;
					}
				}
			}

			FreeLibrary( hDXGI );
		}
	}
	FreeLibrary( hD3D9 );
	return retVal;
}

/*****************************************************************************************
 * getDeviceIdD3D9
 *
 *     Function to get the a device's Vendor ID and Device ID, through the older D3D9 interfaces.
 *
 *****************************************************************************************/

bool getDeviceIdD3D9(     unsigned int* VendorId,
                          unsigned int* DeviceId,
						  int adapterNum = FIRST_GFX_ADAPTER)
{
    /*
    * Does NOT have WDDM Driver. We must be on XP.
    * Let's try using the Direct3DCreate9 function (instead of DXGI)
    */
	bool retVal = false;
    HMODULE hD3D9 = LoadLibrary( "d3d9.dll" );
	if( hD3D9 == NULL )
		return false;
	// Define a function pointer to the Direct3DCreate9 function.
    typedef IDirect3D9* ( WINAPI*LPDIRECT3DCREATE9 )( UINT );

    // Obtain the address of the Direct3DCreate9 function.
    LPDIRECT3DCREATE9 pD3D9Create9 = NULL;
    pD3D9Create9 = ( LPDIRECT3DCREATE9 )GetProcAddress( hD3D9, "Direct3DCreate9" );
    if( pD3D9Create9 )
    {
        // Initialize the D3D9 interface
        LPDIRECT3D9 pD3D = NULL;
        if( ( pD3D = ( *pD3D9Create9 )( D3D_SDK_VERSION ) ) != NULL )
        {
            D3DADAPTER_IDENTIFIER9 adapterDesc;
            // Enumerate adapters. Code here only gets the info for the first adapter.
            if( pD3D->GetAdapterIdentifier( adapterNum, 0, &adapterDesc ) == D3D_OK )
            {
                *VendorId = adapterDesc.VendorId;
                *DeviceId = adapterDesc.DeviceId;

                retVal = true;
            }
            pD3D->Release();
        }
    }
	FreeLibrary( hD3D9 );
	return retVal;
}

/*****************************************************************************************
 * getGraphicsDeviceInfo
 *
 *     Function to get the primary graphics device's Vendor ID and Device ID, either 
 *     through the new DXGI interface or through the older D3D9 interfaces.
 *     The function also returns the amount of memory availble for graphics using 
 *     the value shared + dedicated video memory returned from DXGI, or, if the DXGI
 *	   interface is not available, the amount of memory returned from WMI.
 *
 *****************************************************************************************/

bool getGraphicsDeviceInfo( unsigned int* VendorId,
                          unsigned int* DeviceId,
						  unsigned int* VideoMemory)
{
	bool retVal = false;
    if( ( VendorId == NULL ) || ( DeviceId == NULL ) )
        return retVal;
	
	DXGI_ADAPTER_DESC AdapterDesc;
	if(getDXGIAdapterDesc(&AdapterDesc))
	{
		*VendorId = AdapterDesc.VendorId;
		*DeviceId = AdapterDesc.DeviceId;
		*VideoMemory = (unsigned int)(AdapterDesc.DedicatedVideoMemory + AdapterDesc.SharedSystemMemory);
		retVal = true;
	}
    else
    {
        if(getDeviceIdD3D9(VendorId, DeviceId) && getVideoMemory(VideoMemory))
			retVal = true;
	}
		
    return retVal;
}


/*****************************************************************************************
 * getVideoMemory
 *
 *     Function to find the amount of video memory using the Windows Management Interface
 *	   (WMI), the recommended method for Intel Processor Graphics.
 *
 *****************************************************************************************/

bool getVideoMemory( unsigned int* pVideoMemory )
{
	ULONG mem = 0;
	bool success = false;
	IWbemLocator* pLocator = NULL;
	HRESULT hr = S_OK;
	CoInitialize( 0 );
	hr = CoCreateInstance( CLSID_WbemLocator, NULL, CLSCTX_INPROC_SERVER, IID_IWbemLocator, ( LPVOID* )&pLocator );
	if( S_OK == hr )
	{
		if( pLocator != NULL )
		{
			IWbemServices* pServices = NULL;
			BSTR nameSpace = SysAllocString( L"\\\\.\\root\\cimv2" );
			if( S_OK == pLocator->ConnectServer( nameSpace, NULL, NULL, 0, 0, NULL, NULL, &pServices )
				&& pServices != NULL )
			{
				CoSetProxyBlanket( pServices, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL, RPC_C_AUTHN_LEVEL_CALL, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, 0 );
				IEnumWbemClassObject* pEnumVideo = NULL;
				if( S_OK  == pServices->CreateInstanceEnum( L"Win32_VideoController", 0, NULL, &pEnumVideo )
					&& pEnumVideo != NULL )
				{
					IWbemClassObject* pVideo = NULL;
					DWORD numReturned = 0;
					if( S_OK == pEnumVideo->Next( 1000, 1, &pVideo, &numReturned ) && numReturned == 1 )
					{
						VARIANT v;
						if( S_OK == pVideo->Get( L"AdapterRAM", 0, &v, NULL, NULL ) )
						{
							mem = v.uintVal;
						}
					}
				}
			}
			SysFreeString( nameSpace );
		}
		CoUninitialize();
		success = true;
	}
	*pVideoMemory = mem;
	return success;
}

// The new device dependent counter
#define INTEL_DEVICE_INFO_COUNTERS         "Intel Device Information"

// New device dependent structure
struct SQueryDeviceInfo
{
    DWORD GPUMaxFreq;
    DWORD GPUMinFreq;
};


/******************************************************************************************************************************************
 * getIntelDeviceInfo
 *
 * Description:
 *       Gets device info if available
 *           Supported device info: GPU Max Frequency, GPU Min Frequency, GT Generation, EU Count, Package TDP, Max Fill Rate
 * 
 * Parameters:
 *         unsigned int VendorId                         - [in]     - Input:  system's vendor id
 *         IntelDeviceInfoHeader *pIntelDeviceInfoHeader - [in/out] - Input:  allocated IntelDeviceInfoHeader *
 *                                                                    Output: Intel device info header, if found
 *         void *pIntelDeviceInfoBuffer                  - [in/out] - Input:  allocated void *
 *                                                                    Output: IntelDeviceInfoV[#], cast based on IntelDeviceInfoHeader
 * Return:
 *         GGF_SUCCESS: Able to find Data is valid
 *         GGF_E_UNSUPPORTED_HARDWARE: Unsupported hardware, data is invalid
 *         GGF_E_UNSUPPORTED_DRIVER: Unsupported driver on Intel, data is invalid
 *****************************************************************************************************************************************/

long getIntelDeviceInfo( unsigned int VendorId, IntelDeviceInfoHeader *pIntelDeviceInfoHeader, void *pIntelDeviceInfoBuffer )
{
	// The device information is stored in a D3D counter.
	// We must create a D3D device, find the Intel counter 
	// and query the counter info
	ID3D11Device *pDevice = NULL;
	ID3D11DeviceContext *pImmediateContext = NULL;
	D3D_FEATURE_LEVEL featureLevel;
	HRESULT hr = NULL;

	if ( pIntelDeviceInfoBuffer == NULL )
		return GGF_ERROR;

	if ( VendorId != INTEL_VENDOR_ID )
		return GGF_E_UNSUPPORTED_HARDWARE;

	ZeroMemory( &featureLevel, sizeof(D3D_FEATURE_LEVEL) );

	// First create the Device, must be SandyBridge or later to create D3D11 device
	hr = D3D11CreateDevice( NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, NULL, NULL, NULL,
							D3D11_SDK_VERSION, &pDevice, &featureLevel, &pImmediateContext);

	if ( FAILED(hr) )
	{
		SAFE_RELEASE( pImmediateContext );
		SAFE_RELEASE( pDevice );

		printf("D3D11CreateDevice failed\n");

		return FALSE;
	}
	
	// The counter is in a device dependent counter
	D3D11_COUNTER_INFO counterInfo;
	D3D11_COUNTER_DESC pIntelCounterDesc;
	
	int numDependentCounters;
	UINT uiSlotsRequired, uiNameLength, uiUnitsLength, uiDescLength;
	LPSTR sName, sUnits, sDesc;

	ZeroMemory( &counterInfo, sizeof(D3D11_COUNTER_INFO) );
	ZeroMemory( &pIntelCounterDesc, sizeof(D3D11_COUNTER_DESC) );

	// Query the device to find the number of device dependent counters.
	pDevice->CheckCounterInfo( &counterInfo );

	if ( counterInfo.LastDeviceDependentCounter == 0 )
	{
		SAFE_RELEASE( pImmediateContext );
		SAFE_RELEASE( pDevice );

		printf("No device dependent counters\n");

		// The driver does not support the Device Info Counter.
		return GGF_E_UNSUPPORTED_DRIVER;
	}

	numDependentCounters = counterInfo.LastDeviceDependentCounter - D3D11_COUNTER_DEVICE_DEPENDENT_0 + 1;

	// Search for the apporpriate counter - INTEL_DEVICE_INFO_COUNTERS
	for ( int i = 0; i < numDependentCounters; ++i )
	{
		D3D11_COUNTER_DESC counterDescription;
		D3D11_COUNTER_TYPE counterType;

		counterDescription.Counter = static_cast<D3D11_COUNTER>(i+D3D11_COUNTER_DEVICE_DEPENDENT_0);
		counterDescription.MiscFlags = 0;
		counterType = static_cast<D3D11_COUNTER_TYPE>(0);
		uiSlotsRequired = uiNameLength = uiUnitsLength = uiDescLength = 0;
		sName = sUnits = sDesc = NULL;

		if( SUCCEEDED( hr = pDevice->CheckCounter( &counterDescription, &counterType, &uiSlotsRequired, NULL, &uiNameLength, NULL, &uiUnitsLength, NULL, &uiDescLength ) ) )
		{
			LPSTR sName  = new char[uiNameLength];
			LPSTR sUnits = new char[uiUnitsLength];
			LPSTR sDesc  = new char[uiDescLength];
			
			if( SUCCEEDED( hr = pDevice->CheckCounter( &counterDescription, &counterType, &uiSlotsRequired, sName, &uiNameLength, sUnits, &uiUnitsLength, sDesc, &uiDescLength ) ) )
			{
				if ( strcmp( sName, INTEL_DEVICE_INFO_COUNTERS ) == 0 )
				{
					int IntelCounterMajorVersion;
					int IntelCounterSize;
					int argsFilled = 0;

					pIntelCounterDesc.Counter = counterDescription.Counter;

					argsFilled = sscanf_s( sDesc, "Version %d", &IntelCounterMajorVersion);
					
					if ( argsFilled == 1 )
					{
						sscanf_s( sUnits, "Size %d", &IntelCounterSize);
					}
					else // Fall back to version 1.0
					{
						IntelCounterMajorVersion = 1;
						IntelCounterSize = sizeof( IntelDeviceInfoV1 );
					}

					pIntelDeviceInfoHeader->Version = IntelCounterMajorVersion;
					pIntelDeviceInfoHeader->Size = IntelCounterSize;
				}
			}
			
			SAFE_DELETE_ARRAY( sName );
			SAFE_DELETE_ARRAY( sUnits );
			SAFE_DELETE_ARRAY( sDesc );
		}
	}

	// Check if device info counter was found
	if ( pIntelCounterDesc.Counter == NULL )
	{
		SAFE_RELEASE( pImmediateContext );
		SAFE_RELEASE( pDevice );

		printf("Could not find counter\n");

		// The driver does not support the Device Info Counter.
		return GGF_E_UNSUPPORTED_DRIVER;
	}
	
	// Intel Device Counter //
	ID3D11Counter *pIntelCounter = NULL;

	// Create the appropriate counter
	hr = pDevice->CreateCounter(&pIntelCounterDesc, &pIntelCounter);
	if ( FAILED(hr) )
	{
		SAFE_RELEASE( pIntelCounter );
		SAFE_RELEASE( pImmediateContext );
		SAFE_RELEASE( pDevice );

		printf("CreateCounter failed\n");

		return GGF_E_D3D_ERROR;
	}

	// Begin and end counter capture
	pImmediateContext->Begin(pIntelCounter);
	pImmediateContext->End(pIntelCounter);

	// Check for available data
	hr = pImmediateContext->GetData( pIntelCounter, NULL, NULL, NULL );
	if ( FAILED(hr) || hr == S_FALSE )
	{
		SAFE_RELEASE( pIntelCounter );
		SAFE_RELEASE( pImmediateContext );
		SAFE_RELEASE( pDevice );

		printf("Getdata failed \n");
		return GGF_E_D3D_ERROR;
	}
	
	DWORD pData[2] = {0};
	// Get pointer to structure
	hr = pImmediateContext->GetData(pIntelCounter, pData, 2*sizeof(DWORD), NULL);

	if ( FAILED(hr) || hr == S_FALSE )
	{
		SAFE_RELEASE( pIntelCounter );
		SAFE_RELEASE( pImmediateContext );
		SAFE_RELEASE( pDevice );

		printf("Getdata failed \n");
		return GGF_E_D3D_ERROR;
	}

	//
	// Prepare data to be returned //
	//
	// Copy data to passed in parameter
	void *pDeviceInfoBuffer = *(void**)pData;

	memcpy( pIntelDeviceInfoBuffer, pDeviceInfoBuffer, pIntelDeviceInfoHeader->Size );

	//
	// Clean up //
	//
	SAFE_RELEASE( pIntelCounter );
	SAFE_RELEASE( pImmediateContext );
	SAFE_RELEASE( pDevice );

	return GGF_SUCCESS;
}
