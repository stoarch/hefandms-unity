//--------------------------------------------------------------------------------------
// Copyright 2013 Intel Corporation
// All Rights Reserved
//
// Permission is granted to use, copy, distribute and prepare derivative works of this
// software for any purpose and without fee, provided, that the above copyright notice
// and this statement appear in all copies.  Intel makes no representations about the
// suitability of this software for any purpose.  THIS SOFTWARE IS PROVIDED "AS IS."
// INTEL SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, AND ALL LIABILITY,
// INCLUDING CONSEQUENTIAL AND OTHER INDIRECT DAMAGES, FOR THE USE OF THIS SOFTWARE,
// INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PROPRIETARY RIGHTS, AND INCLUDING THE
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  Intel does not
// assume any responsibility for any errors which may appear in this software nor any
// responsibility to update it.
//--------------------------------------------------------------------------------------
#ifndef __CPUT_WINDOWS_MOBILITY_APIS_H__
#define __CPUT_WINDOWS_MOBILITY_APIS_H__

#include "windows.h"

extern "C"
{
#include <Powrprof.h>
}


enum PowerSource
{
	Battery_Power,
	AC_Power,
	Undefined_Power = 255
};

class MobilityMonitor
{
	// private variable to store power status information
	SYSTEM_POWER_STATUS pwrStat;
	bool isValid;
	UCHAR PowerSchemeName[1024];
	void Poll();

public:
	MobilityMonitor();
	~MobilityMonitor();

	bool IsLaptop();
	PowerSource GetPowerSrc();
	unsigned long GetSecBatteryLifeTimeRemaining();
	int GetPercentBatteryLife();
	void ReadCurrentScheme();
	WCHAR* GetPowerSchemeName(){ return (WCHAR*)PowerSchemeName;}
	int Register(HWND hWnd);

};


#endif