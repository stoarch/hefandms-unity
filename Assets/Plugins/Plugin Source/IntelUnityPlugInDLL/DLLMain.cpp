//--------------------------------------------------------------------------------------
// Copyright 2011 Intel Corporation
// All Rights Reserved
//
// Permission is granted to use, copy, distribute and prepare derivative works of this
// software for any purpose and without fee, provided, that the above copyright notice
// and this statement appear in all copies.  Intel makes no representations about the
// suitability of this software for any purpose.  THIS SOFTWARE IS PROVIDED "AS IS."
// INTEL SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, AND ALL LIABILITY,
// INCLUDING CONSEQUENTIAL AND OTHER INDIRECT DAMAGES, FOR THE USE OF THIS SOFTWARE,
// INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PROPRIETARY RIGHTS, AND INCLUDING THE
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  Intel does not
// assume any responsibility for any errors which may appear in this software nor any
// responsibility to update it.
//--------------------------------------------------------------------------------------

#include "windows.h"
#include "windowsx.h"
#include "CommCtrl.h"
#include "stdio.h"
#include "DeviceId.h"
#include "WindowsMobilityAPIs.h"
#include <iostream>
#include <fstream>
#include <Shobjidl.h>

// Avoid Name Mangling
extern "C"
{

// Global Handles
HWND g_hRemoteWnd;
HINSTANCE g_hInst;
HHOOK g_HookPosted = NULL;
HHOOK g_HookCalled = NULL;
MobilityMonitor g_MobMonitor;

#define MAX_TOUCH 128

// Storage for touch tracking
struct tTouchData
{
	int m_x;
	int m_y;
	int m_ID;
	int m_Time;
	int m_RelativeX;
	int m_RelativeY;
};

tTouchData	g_TouchData[MAX_TOUCH];
tTouchData	g_CopyTouchPoints[MAX_TOUCH];

// Clear all to -1 IDs at start
void ClearData()
{
	for (int i=0;i<MAX_TOUCH;i++)
	{
		g_TouchData[i].m_ID = -1;
	}
}

// On Touch function - from MSDN somewhere
// Added code to store touch info into global storage
// WARNING - ERRORS NOT HANDLED!!!!!
LRESULT OnTouch(HWND hWnd, WPARAM wParam, LPARAM lParam )
{
	BOOL bHandled = FALSE;
	UINT cInputs = LOWORD(wParam);
	PTOUCHINPUT pInputs = new TOUCHINPUT[cInputs];

	if (pInputs)
	{
		//get the window size so we can compensate if necessary
		RECT ScreenRect, ClientRect;
		int XDiff = 0;
		int YDiff = 0;
		GetWindowRect(g_hRemoteWnd, &ScreenRect);
		GetClientRect(g_hRemoteWnd, &ClientRect);
		XDiff = (ScreenRect.right - ScreenRect.left) - ClientRect.right;
		YDiff = (ScreenRect.bottom - ScreenRect.top) - ClientRect.bottom;

		if (GetTouchInputInfo((HTOUCHINPUT)lParam, cInputs, pInputs, sizeof(TOUCHINPUT)))
		{
			for (UINT i=0; i < cInputs; i++)
			{
				TOUCHINPUT ti = pInputs[i];

				if (ti.dwFlags & TOUCHEVENTF_DOWN)	// Add to list
				{
					for (int p=0;p<MAX_TOUCH;p++)
					{
						if (g_TouchData[p].m_ID == -1)
						{
							g_TouchData[p].m_ID = ti.dwID;
							g_TouchData[p].m_x = ti.x;
							g_TouchData[p].m_y = ti.y;
							g_TouchData[p].m_Time = ti.dwTime;
							g_TouchData[p].m_RelativeX = (ti.x / 100) - ScreenRect.left - XDiff;
							g_TouchData[p].m_RelativeY = (ti.y / 100) - ScreenRect.top - YDiff;
							break;
						}
					}
				}
				if (ti.dwFlags & TOUCHEVENTF_UP)	// Remove from list
				{
					for (int p=0;p<MAX_TOUCH;p++)
					{
						if (g_TouchData[p].m_ID == ti.dwID)
						{
							g_TouchData[p].m_ID = -1;
							break;
						}
					}
				}
				if (ti.dwFlags & TOUCHEVENTF_MOVE)	// Move point
				{
					for (int p=0;p<MAX_TOUCH;p++)
					{
						if (g_TouchData[p].m_ID == ti.dwID)
						{
							g_TouchData[p].m_x = ti.x;
							g_TouchData[p].m_y = ti.y;
							g_TouchData[p].m_Time = ti.dwTime;
							g_TouchData[p].m_RelativeX = (ti.x / 100) - ScreenRect.left - XDiff;
							g_TouchData[p].m_RelativeY = (ti.y / 100) - ScreenRect.top - YDiff;
							break;
						}
					}
				}
			}            
			bHandled = TRUE;
		}
		else
		{
			/* handle the error here */
		}
		delete [] pInputs;
	}
	else
	{
		/* handle the error here, probably out of memory */
	}
	if (bHandled)
	{
		// if you handled the message, close the touch input handle and return
		CloseTouchInputHandle((HTOUCHINPUT)lParam);
		return 0;
	}
	else
	{
		// if you didn't handle the message, let DefWindowProc handle it
		return DefWindowProc(hWnd, WM_TOUCH, wParam, lParam);
	}
}

// Hook prok to capture calls to Unity Window function by System
LRESULT HookProcCalled(int code, WPARAM wParam, LPARAM lParam)
{
	CWPSTRUCT *Msg = (CWPSTRUCT *)lParam;
	if (code >= 0)
	{
		if (Msg->hwnd == g_hRemoteWnd)
		{
			switch (Msg->message)
			{
			case WM_TOUCH:
				OnTouch(Msg->hwnd, Msg->wParam, Msg->lParam);
				break;
			}
		}
	}
	// Always call this - we dont actually "Use" any of the messages
	return CallNextHookEx(0, code, wParam, lParam); 
}


// Hook prok to capture messages Posted to Unity Window
LRESULT HookProcPosted(int code, WPARAM wParam, LPARAM lParam)
{
	MSG *Msg = (MSG *)lParam;
	if (code >= 0)
	{
		if (Msg->hwnd == g_hRemoteWnd)
		{
			switch (Msg->message)
			{
			case WM_TOUCH:
				OnTouch(Msg->hwnd, Msg->wParam, Msg->lParam);
				break;
			}
		}
	}
	// Always call this - we dont actually "Use" any of the messages
	return CallNextHookEx(0, code, wParam, lParam); 
}
struct tWindowData
{
	char *pName;
	HWND Handle;
};

// loop through the Windows on the desktop,
// Stop when we find the Unity Window
BOOL CALLBACK EnumWindowsFunc(
  _In_  HWND hwnd,
  _In_  LPARAM lParam
)
{
	tWindowData *pData = (tWindowData *)lParam;
	char NewName[128];
	GetWindowText(hwnd, NewName, 128);
	if (!strcmp(pData->pName, NewName))
	{
		pData->Handle = hwnd;
		return false;
	}
	return true;

}

struct DeviceInfo
{
	unsigned int VendorId;
	unsigned int DeviceId;
	unsigned int VideoMemory;
};


__declspec(dllexport) IntelDeviceInfo __cdecl GetDeviceInfo(void)
{

	IntelDeviceInfo intelDeviceInfo;
	memset(&intelDeviceInfo, 0, sizeof(IntelDeviceInfo));

	// Retrieve Intel device information
	IntelDeviceInfoHeader intelDeviceInfoHeader = { 0 };
	byte intelDeviceInfoBuffer[1024];

	long getStatus = getIntelDeviceInfo( INTEL_VENDOR_ID, &intelDeviceInfoHeader, &intelDeviceInfoBuffer );

	if ( getStatus == GGF_SUCCESS )
	{
		//const char *PRODUCT_FAMILY_STRING[] =
		//{
		//	"Sandy Bridge",
		//	"Ivy Bridge",
		//	"Haswell",
		//};


		if ( intelDeviceInfoHeader.Version == 2 )
		{
			IntelDeviceInfoV2 idi2;

			memcpy( &idi2, intelDeviceInfoBuffer, intelDeviceInfoHeader.Size );
			intelDeviceInfo.GPUMaxFreq = idi2.GPUMaxFreq;
			intelDeviceInfo.GPUMinFreq = idi2.GPUMinFreq;
			intelDeviceInfo.EUCount = idi2.EUCount;
			intelDeviceInfo.GTGeneration = idi2.GTGeneration;
			intelDeviceInfo.MaxFillRate = idi2.MaxFillRate;
			intelDeviceInfo.PackageTDP = idi2.PackageTDP;

			//int myGeneration = idi2.GTGeneration;
			//int generationIndex = myGeneration - IGFX_SANDYBRIDGE;
			//const char *myGenerationString;

			//if ( myGeneration < IGFX_SANDYBRIDGE )
			//	myGenerationString = "Old unrecognized generation";
			//else if ( myGeneration > IGFX_HASWELL )
			//	myGenerationString = "New unrecognized generation";
			//else
			//	myGenerationString = PRODUCT_FAMILY_STRING[generationIndex];

		}
		else
		{
			IntelDeviceInfoV1 idi1;
			memcpy( &idi1, intelDeviceInfoBuffer, intelDeviceInfoHeader.Size );
			intelDeviceInfo.GPUMaxFreq = idi1.GPUMaxFreq;
			intelDeviceInfo.GPUMinFreq = idi1.GPUMinFreq;
		}
	}
	else if ( getStatus == GGF_E_UNSUPPORTED_HARDWARE )
	{
		//we could do something else here but the struct is already filled with zeros
	}

	return intelDeviceInfo;

}


__declspec(dllexport) int __cdecl GetDeviceId(void)
{
	unsigned int VendorId, DeviceId, VideoMemory;

	if( getGraphicsDeviceInfo( &VendorId, &DeviceId, &VideoMemory ) )
	{
		return DeviceId;
	}

	return 0;
}

//
// BOOL IsVirtualKeyboardVisible()
//
// Returns TRUE if Virtual Keyboard/Input Pane is visible
// Returns FALSE if Virtual Keyboard/Input Pane is not visible

__declspec(dllexport) BOOL __cdecl IsVirtualKeyboardVisible()
{
	BOOL	bRet = FALSE;
	RECT	InputPaneScreenLocation = { 0, 0, 0, 0 };
	/*
	__try
	{
		HRESULT hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);

		IFrameworkInputPane *IinputPane = NULL;

		if (SUCCEEDED(hr))
		{
			//
			// http://msdn.microsoft.com/en-us/library/windows/desktop/hh706967(v=vs.85).aspx
			//
			hr = CoCreateInstance(__uuidof(FrameworkInputPane), 0, CLSCTX_ALL, __uuidof(IFrameworkInputPane), (LPVOID*)&IinputPane);
			IinputPane->Location(&InputPaneScreenLocation);

			if (InputPaneScreenLocation.bottom == 0 && InputPaneScreenLocation.left == 0 &&
				InputPaneScreenLocation.right == 0 && InputPaneScreenLocation.top == 0)
			{
				// VKB is not visible
				bRet = FALSE;
			}
			else
			{
				// VKB is visible
				bRet = TRUE;
			} 
		}

	}	// try
	__finally
	{
		CoUninitialize();
	}
	*/
	return bRet;
}

__declspec(dllexport) bool __cdecl IsLaptop(void)
{
	return ( g_MobMonitor.IsLaptop() );
}

__declspec(dllexport) int __cdecl GetPercentBatteryLife(void)
{
	return ( g_MobMonitor.GetPercentBatteryLife() );
}

__declspec(dllexport) int __cdecl GetBatteryMode(void)
{
	return ( (int)g_MobMonitor.GetPowerSrc() );
}

BOOL setval = false;
// exported func to set up the hook
// NOTE: Currently assuming WCHAR array comming from Unity.
// Earlier versions used char arrays.  For older versions of Unity convert to char * by
// WindowData.pName to the parameter instead of converting
// or do something cleverer!
__declspec(dllexport) int __cdecl Initialise(WCHAR *PName)
{
	char Str[128];
	int Len = lstrlenW(PName);
	WideCharToMultiByte(CP_ACP, 0, PName, Len, Str, 127, NULL, NULL);
	Str[Len] = 0;

	tWindowData WindowData;
	WindowData.pName = Str;
	WindowData.Handle = NULL;
	EnumDesktopWindows(NULL, EnumWindowsFunc, (LPARAM)&WindowData);
	if (WindowData.Handle == NULL)
		return -1;
	g_hRemoteWnd = WindowData.Handle; 

	// Get thread ID for Unity window
	DWORD ProcessID;
	int ID = GetWindowThreadProcessId(g_hRemoteWnd, &ProcessID);  
	if (ID == 0)
		return -2;
	
	// Set the Hooks
	// One for Posted Messages
	g_HookPosted = SetWindowsHookEx(WH_GETMESSAGE, (HOOKPROC)HookProcPosted, g_hInst, ID); 
	if (g_HookPosted == NULL)
	{
		return -3;
	}

	// One hook for System calls (Win8)
	g_HookCalled = SetWindowsHookEx(WH_CALLWNDPROC, (HOOKPROC)HookProcCalled, g_hInst, ID); 
	if (g_HookCalled == NULL)
	{
		UnhookWindowsHookEx(g_HookPosted);
		return -4;
	}

	// Setup our data
	ClearData();	

	// Register the Unity window for touch
	int Res = RegisterTouchWindow(g_hRemoteWnd, TWF_WANTPALM); 
	if (Res == 0)
	{
		UnhookWindowsHookEx(g_HookPosted);
		UnhookWindowsHookEx(g_HookCalled);
		return -5;
	}

	if(g_MobMonitor.Register( g_hRemoteWnd ) == 0)
		return -6;
	
	//in win8 sdk header
	typedef enum tagFEEDBACK_TYPE { 
		FEEDBACK_TOUCH_CONTACTVISUALIZATION  = 1,
		FEEDBACK_PEN_BARRELVISUALIZATION     = 2,
		FEEDBACK_PEN_TAP                     = 3,
		FEEDBACK_PEN_DOUBLETAP               = 4,
		FEEDBACK_PEN_PRESSANDHOLD            = 5,
		FEEDBACK_PEN_RIGHTTAP                = 6,
		FEEDBACK_TOUCH_TAP                   = 7,
		FEEDBACK_TOUCH_DOUBLETAP             = 8,
		FEEDBACK_TOUCH_PRESSANDHOLD          = 9,
		FEEDBACK_TOUCH_RIGHTTAP              = 10,
		FEEDBACK_GESTURE_PRESSANDTAP         = 11,
		FEEDBACK_MAX                         = 0xFFFFFFFF
	} FEEDBACK_TYPE ;

	//check if win8 function exists
	//if so turn off the windows 8 touch feedback stuff
	typedef BOOL (*LPSETWINFEEDBACK)(HWND hwnd, FEEDBACK_TYPE feedback, DWORD dwFlags, UINT32 size, const VOID *configuration);
	HINSTANCE hDLL = NULL;
	LPSETWINFEEDBACK lpSetWinFeedBack;
	hDLL = LoadLibrary("User32.DLL");
	lpSetWinFeedBack = (LPSETWINFEEDBACK)GetProcAddress((HMODULE)hDLL, "SetWindowFeedbackSetting");

	if( lpSetWinFeedBack )
	{
		setval = FALSE;
		if(lpSetWinFeedBack( g_hRemoteWnd, FEEDBACK_TOUCH_CONTACTVISUALIZATION, 0, sizeof(BOOL), (void*)&setval) == false)
			return -7;
		setval = FALSE;
		if(lpSetWinFeedBack( g_hRemoteWnd, FEEDBACK_TOUCH_TAP, 0, sizeof(BOOL), (void*)&setval) == false)
			return -7;
		setval = FALSE;
		if(lpSetWinFeedBack( g_hRemoteWnd, FEEDBACK_TOUCH_PRESSANDHOLD, 0, sizeof(BOOL), (void*)&setval) == false)
			return -7;
		setval = FALSE;
		if(lpSetWinFeedBack( g_hRemoteWnd, FEEDBACK_TOUCH_DOUBLETAP, 0, sizeof(BOOL), (void*)&setval) == false)
			return -7;
		setval = FALSE;
		if(lpSetWinFeedBack( g_hRemoteWnd, FEEDBACK_TOUCH_RIGHTTAP, 0, sizeof(BOOL), (void*)&setval) == false)
			return -7;
		setval = FALSE;
		if(lpSetWinFeedBack( g_hRemoteWnd, FEEDBACK_GESTURE_PRESSANDTAP, 0, sizeof(BOOL), (void*)&setval) == false)
			return -7;
	}
	else
	{
		return -7;
	}

	return 0;
}

__declspec(dllexport) void __cdecl ShutDown(char *PName)
{
	UnhookWindowsHookEx(g_HookPosted);
	UnhookWindowsHookEx(g_HookCalled);
}

// Fairly self explanatory exported funcs

__declspec(dllexport) void __cdecl GetTouchPoint(int i, tTouchData *p)
{
	*p = g_CopyTouchPoints[i];
}

__declspec(dllexport) int __cdecl GetTouchPointCount()
{
	int NumPoints = 0;
	for (int i=0;i<MAX_TOUCH;i++)
	{
		if (g_TouchData[i].m_ID != -1)
		{
			g_CopyTouchPoints[NumPoints] = g_TouchData[i];
			NumPoints++;
		}
	}
	return NumPoints;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	g_hInst = hModule;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

}